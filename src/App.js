import * as React from 'react';
import {SafeAreaView, Text} from 'react-native';

// Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Components
import FAIcon from 'react-native-vector-icons/FontAwesome';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';

// Main Views
import SplashScreen from './views/splash';
import HomeScreen from './views/home';
import PayslipScreen from './views/payslip';
import HistoryScreen from './views/history';
import ProfileScreen from './views/profile';
import LoginScreen from './views/login';

// Account Page
import EditProfileScreen from './views/account/editProfile';
import ChangePasswordScreen from './views/account/changePassword';

// Help Page
import TACScreen from './views/help/tac';
import PPScreen from './views/help/pp';
import FAQScreen from './views/help/faq';
import ContactScreen from './views/help/contact';

// Floating Button
import ClockIn from './views/clockin';
import Overtime from './views/overtime';
import Absent from './views/absent';
import Task from './views/task';
import AuthContext from './AuthContext';

// Others
import {List, Detail} from './components/feeds';

// Services
import server from './services/server';

function KPIScreen() {
  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Comming Soon in v2.1.0 (Jan 2021)</Text>
    </SafeAreaView>
  );
}

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function AppStack() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({color}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'Payslip') {
            iconName = 'cash-multiple';
          } else if (route.name == 'History') {
            iconName = 'clipboard-check-outline';
          } else if (route.name == 'KPI') {
            return <FAIcon name={'line-chart'} size={20} color={color} />;
          } else if (route.name == 'Profile') {
            return <FAIcon name={'user-circle'} size={20} color={color} />;
          }

          return <MCIcon name={iconName} size={24} color={color} />;
        },
      })}
      tabBarOptions={{
        labelStyle: {fontSize: 14, fontWeight: 'bold'},
        activeBackgroundColor: '#D3A40E',
        inactiveBackgroundColor: '#faf5e6',
        activeTintColor: '#faf5e6',
        inactiveTintColor: '#636363',
      }}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Payslip" component={PayslipScreen} />
      <Tab.Screen name="History" component={HistoryScreen} />
      {/* <Tab.Screen name="KPI" component={KPIScreen} /> */}
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
}

const initialState = {
  isReady: false,
  token: null,
  employee: null,
  lessSecure: false,
  task: 0,
};

function reducer(state, action) {
  switch (action.type) {
    case 'RESTORE_TOKEN':
    case 'SIGN_IN':
      if (action.token != null) AsyncStorage.setItem('token', action.token);
      return {
        ...state,
        isReady: true,
        token: action.token,
        employee: action.employee,
        lessSecure: action.lessSecure,
        task: action.task,
      };
    case 'RELOAD_EMPLOYEE':
      return {
        ...state,
        isReady: true,
        employee: action.employee,
        task: action.task,
      };
    case 'SIGN_OUT':
      AsyncStorage.removeItem('token');
      return {
        ...state,
        isReady: true,
        token: null,
        employee: null,
        lessSecure: false,
        task: 0,
      };
  }
}

function App() {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  React.useEffect(() => {
    const restoreToken = async () => {
      await sleepTask();

      let token = await AsyncStorage.getItem('token');
      if (token != null) {
        // Validate in the server
        server.defaults.headers.common = {Authorization: `Bearer ${token}`};
        await server
          .get('employees/me')
          .then((response) => {
            let employee = response.data.data.data;
            let lessSecure = response.data.data.lessSecure;
            let task = response.data.data.task;
            dispatch({
              type: 'SIGN_IN',
              token: token,
              employee: employee,
              lessSecure: lessSecure,
              task: task,
            });
            return;
          })
          .catch((error) => {
            console.log(error.response.data.error);
            dispatch({type: 'SIGN_OUT'});
          });
      } else {
        dispatch({type: 'SIGN_OUT'});
      }
    };

    if (!state.isReady) restoreToken();
  }, [state]);

  sleepTask = async () => {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve('result');
      }, 500),
    );
  };

  if (!state.isReady) {
    return <SplashScreen />;
  }

  return (
    <AuthContext.Provider value={{state, dispatch}}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: '#D3A40E',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}>
          <Stack.Screen
            name="Home"
            component={AppStack}
            options={{headerShown: false}}
          />

          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{headerShown: false}}
          />

          <Stack.Screen name="Edit Profile" component={EditProfileScreen} />
          <Stack.Screen
            name="Change Password"
            component={ChangePasswordScreen}
          />

          <Stack.Screen
            name="TAC"
            component={TACScreen}
            options={{title: 'Term and Condition'}}
          />
          <Stack.Screen
            name="PP"
            component={PPScreen}
            options={{title: 'Privacy and Policy'}}
          />
          <Stack.Screen
            name="FAQ"
            component={FAQScreen}
            options={{title: 'Frequently Asked Questions'}}
          />
          <Stack.Screen
            name="Contact"
            component={ContactScreen}
            options={{title: 'Contact Us'}}
          />

          <Stack.Screen
            name="News"
            component={List}
            options={{title: 'News Update'}}
          />
          <Stack.Screen
            name="Training"
            component={List}
            options={{title: 'Training List'}}
          />
          <Stack.Screen
            name="FeedDetail"
            component={Detail}
            options={{title: 'Detail'}}
          />

          <Stack.Screen
            name="ClockIn"
            component={ClockIn}
            options={{title: 'Clock In'}}
          />

          <Stack.Screen
            name="Overtime"
            component={Overtime}
            options={{title: 'Overtime'}}
          />

          <Stack.Screen
            name="Absent"
            component={Absent}
            options={{title: 'Absent'}}
          />

          <Stack.Screen
            name="Task"
            component={Task}
            options={{title: 'Task'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}

export default App;
