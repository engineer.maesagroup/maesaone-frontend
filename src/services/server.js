import axios from 'axios';

const server = axios.create({
  baseURL: 'https://www.maesaone.com/api/',
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
    'User-Agent': 'API_TEST',
  },
});

export default server;
