import axios from 'axios';

const maesagroup = axios.create({
  baseURL: 'https://maesagroup.co.id/wp-json/wp/v2/',
  timeout: 20000,
});

export default maesagroup;
