import {StyleSheet, Dimensions} from 'react-native';
const {width: WIDTH} = Dimensions.get('screen');
const menuWidth = (WIDTH - 50) / 4;

const styles = StyleSheet.create({
  container: {
    width: WIDTH - 40,
    height: 80,
    alignSelf: 'center',
    marginTop: 210,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    position: 'absolute',
    borderRadius: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#D3A40E',
    borderWidth: 1,
  },
  menu1: {
    width: menuWidth,
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderColor: '#D3A40E',
  },
  menu2: {
    width: menuWidth,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    fontSize: 26,
    marginBottom: 5,
    color: '#D3A40E',
    alignSelf: 'center',
  },
  title: {
    fontSize: 14,
    color: '#D3A40E',
    fontWeight: 'bold',
  },

  badge: {
    borderRadius: 9,
    height: 18,
    minWidth: 0,
    width: 18,
  },
  badgeContainer: {
    position: 'absolute',
  },
  badgeText: {
    fontSize: 10,
    paddingHorizontal: 0,
  },
});

export default styles;
