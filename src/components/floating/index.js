import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import {Badge} from 'react-native-elements';
import styles from './styles';
import {useFocusEffect} from '@react-navigation/native';

// Components
import FAIcon from 'react-native-vector-icons/FontAwesome';
import AuthContext from '../../AuthContext';

const floating = ({navigation}) => {
  const {state} = React.useContext(AuthContext);
  const [task, setTask] = React.useState(0);

  React.useEffect(() => {
    if (state != null && state.employee != null) {
      setTask(state.task);
    } else {
      setTask(0);
    }
  }, [state]);

  useFocusEffect(
    React.useCallback(() => {
      if (state != null && state.employee != null) {
        setTask(state.task);
      } else {
        setTask(0);
      }
    }, [state]),
  );

  // console.log(navigation);
  return (
    <View style={styles.container}>
      <View style={styles.menu1}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('ClockIn');
          }}>
          <FAIcon style={styles.icon} name={'calendar'} />
          <Text style={styles.title}>Clock In</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menu1}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Overtime');
          }}>
          <FAIcon style={styles.icon} name={'clock-o'} />
          <Text style={styles.title}>Overtime</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menu1}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Absent');
          }}>
          <FAIcon style={styles.icon} name={'calendar-times-o'} />
          <Text style={styles.title}>Absent</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menu2}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Task');
          }}>
          <View>
            <FAIcon style={styles.icon} name={'tasks'} />
            {task != 0 ? (
              <Badge
                value={task}
                status="success"
                containerStyle={{position: 'absolute', top: -10, right: -10}}
              />
            ) : null}
          </View>
          <Text style={styles.title}>Task</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default floating;
