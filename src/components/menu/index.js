import React, {Component} from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import styles from './styles';

// Components
import FAIcon from 'react-native-vector-icons/FontAwesome';

class Group extends Component {
  render = () => {
    return (
      <View style={this.props.styles}>
        <Text style={styles.grouping}>{this.props.text}</Text>
      </View>
    );
  };
}
class Item extends Component {
  render = () => {
    return (
      <TouchableOpacity style={styles.link} onPress={this.props.onPress}>
        <Text>{this.props.text}</Text>
        <FAIcon style={styles.icon} name={'chevron-circle-right'} />
      </TouchableOpacity>
    );
  };
}

export {Group, Item};
