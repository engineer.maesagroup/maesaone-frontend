import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  grouping: {
    fontSize: 18,
    fontWeight: '600',
    padding: 10,
    backgroundColor: '#f2e4bc',
  },
  link: {
    padding: 10,
    borderWidth: 1,
    borderColor: '#f2e4bc',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default styles;
