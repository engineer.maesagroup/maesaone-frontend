import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  Text,
  ImageBackground,
  Image,
} from 'react-native';
import styles from './styles';

// Components
import Floating from '../floating';
import AuthContext from '../../AuthContext';

// Assets
import backgroundImage from '../../assets/images/dashboard.jpg';

function gravatar({navigation}) {
  const {state} = React.useContext(AuthContext);

  const [name, setName] = React.useState('Guest');
  const [company, setCompany] = React.useState('Please Sign In');
  const [photo, setPhoto] = React.useState({
    uri: 'https://www.maesaone.com/assets/images/user.png',
  });

  React.useEffect(() => {
    if (state != null && state.employee != null) {
      setName(state.employee.name);
      setCompany(state.employee.company);
      if (state.employee.photo != null)
        setPhoto({uri: `https://www.maesaone.com/${state.employee.photo}`});
      else setPhoto({uri: 'https://www.maesaone.com/assets/images/user.png'});
    } else {
      setName('Guest');
      setCompany('Please Sign In');
      setPhoto({uri: 'https://www.maesaone.com/assets/images/user.png'});
    }
  }, [state]);

  handlePress = () => {
    navigation.navigate('Profile');
  };

  return (
    <ImageBackground source={backgroundImage} style={styles.container}>
      <SafeAreaView>
        <TouchableOpacity onPress={this.handlePress} style={styles.content}>
          <View style={[styles.avatar, styles.avatarContainer, {margin: 10}]}>
            <Image style={styles.avatar} source={photo} />
          </View>
          <View style={styles.employeeContainer}>
            <Text numberOfLines={1} style={styles.name}>
              {name}
            </Text>
            <Text style={styles.companyName}>{company}</Text>
          </View>
        </TouchableOpacity>
        <Floating navigation={navigation} />
      </SafeAreaView>
    </ImageBackground>
  );
}

export default gravatar;
