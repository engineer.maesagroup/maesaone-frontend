import React from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  Image,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import {WebView} from 'react-native-webview';
import styles from './styles';
import Moment from 'moment';

// Components
import FAIcon from 'react-native-vector-icons/FontAwesome';

// Services
import duatangan from '../../services/duatangan';
import maesagroup from '../../services/maesagroup';

const Item = ({id, background, date, title, type, navigation}) => {
  return (
    <View>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{uri: background}}
          resizeMode="cover"
          resizeMethod="resize"
        />
        <View style={styles.timeContainer}>
          <Text style={styles.time}>{date}</Text>
        </View>
      </View>
      <View style={styles.bottomContainer}>
        <View style={styles.subtitleContainer}>
          <Text numberOfLines={2} style={styles.subtitleText}>
            {title}
          </Text>
        </View>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() =>
            navigation.navigate('FeedDetail', {
              id: id,
              title: title,
              type: type,
            })
          }>
          <View style={styles.buttonStyle}>
            <Text style={styles.buttonText}>Detail..</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const List = ({route, navigation}) => {
  const [listData, setListData] = React.useState(null);
  const [type, setType] = React.useState('');
  const mountedRef = React.useRef(true);

  React.useEffect(() => {
    const refreshData = async () => {
      try {
        if (route.params?.type == 'News') {
          await maesagroup
            .get('posts?_embed&per_page=15')
            .then((response) => {
              if (!mountedRef.current) return null;
              setListData(response.data);
              setType('News');
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });
        }
        if (route.params?.type == 'Training') {
          await duatangan
            .get('trainings/fetchNewTraining')
            .then((response) => {
              if (!mountedRef.current) return null;
              setListData(response.data);
              setType('Training');
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });
        }
      } catch (error) {
        console.error(error);
      }
    };

    if (listData == null) refreshData();

    return () => {
      mountedRef.current = false;
    };
  }, []);

  keyExtractor = (item) => {
    return route.params?.type == 'News'
      ? item.id.toString()
      : item.id_training.toString();
  };

  renderItem = ({item}) => {
    if (type == 'News')
      return (
        <View style={styles.cardContainer}>
          <Item
            id={item.id.toString()}
            background={item._embedded['wp:featuredmedia']['0'].source_url}
            date={Moment(item.date).format('DD MMM YYYY')}
            title={item.title.rendered}
            type={type}
            navigation={navigation}
          />
        </View>
      );
    if (type == 'Training')
      return (
        <View style={styles.cardContainer}>
          <Item
            id={item.id_training.toString()}
            background={item.banner}
            date={Moment(item.mulai).format('DD MMM YYYY')}
            title={item.nama}
            type={type}
            navigation={navigation}
          />
        </View>
      );
    return null;
  };

  return (
    <SafeAreaView style={styles.container}>
      {listData == null ? (
        <ActivityIndicator size="large" color="gold" />
      ) : (
        <FlatList
          data={listData}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      )}
    </SafeAreaView>
  );
};

const Detail = ({route}) => {
  const [title, setTitle] = React.useState(null);
  const [date, setDate] = React.useState(null);
  const [image, setImage] = React.useState('');
  const [content, setContent] = React.useState('');
  const [uri, setUri] = React.useState('');
  const mountedRef = React.useRef(true);

  React.useEffect(() => {
    const refreshData = async () => {
      try {
        if (route.params?.type == 'News') {
          await maesagroup
            .get('posts/' + route.params?.id + '?_embed')
            .then((response) => {
              if (!mountedRef.current) return null;
              setTitle(response.data.title.rendered);
              setDate(Moment(response.data.date).format('DD MMM YYYY'));
              setImage(
                response.data._embedded['wp:featuredmedia']['0'].source_url,
              );
              setContent(
                `<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>${response.data.content.rendered}</body></html>`,
              );
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });
        }
        if (route.params?.type == 'Training') {
          if (!mountedRef.current) return null;
          console.log(route.params?.id);
          setUri(
            `https://training.dua-tangan.com/training/detail/${route.params?.id}`,
          );
        }
      } catch (error) {
        console.error(error);
      }
    };

    if (title == null) refreshData();

    return () => {
      mountedRef.current = false;
    };
  }, []);

  return (
    <View style={[styles.container, {marginTop: 0}]}>
      {route.params?.type == 'News' ? (
        <SafeAreaView style={styles.detailContainer}>
          <Text style={[styles.titleText, styles.detailTitle]}>{title}</Text>
          <Text style={styles.timeText}>{date}</Text>
          {image == '' ? (
            <ActivityIndicator size="large" color="gold" />
          ) : (
            <Image style={styles.imageHtml} source={{uri: image}} />
          )}
          <View style={styles.htmlContent}>
            <WebView
              javaScriptEnabled
              domStorageEnabled
              allowFileAccessFromFileURLs
              startInLoadingState
              originWhitelist={['*']}
              mixedContentMode="compatibility"
              source={{html: content}}
              style={styles.html}
            />
          </View>
        </SafeAreaView>
      ) : (
        <SafeAreaView style={styles.detailContainer}>
          {uri == '' ? (
            <ActivityIndicator size="large" color="gold" />
          ) : (
            <View style={styles.htmlContent}>
              <WebView
                javaScriptEnabled
                domStorageEnabled
                allowFileAccessFromFileURLs
                startInLoadingState
                originWhitelist={['*']}
                mixedContentMode="compatibility"
                source={{uri: uri}}
                style={styles.html}
              />
            </View>
          )}
        </SafeAreaView>
      )}
    </View>
  );
};

const Feeds = (props) => {
  const [listData, setListData] = React.useState();
  const mountedRef = React.useRef(true);

  React.useEffect(() => {
    const refreshData = async () => {
      try {
        if (props.type == 'News') {
          await maesagroup
            .get('posts?_embed&per_page=5')
            .then((response) => {
              if (!mountedRef.current) return null;
              setListData(response.data);
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });
        }
        if (props.type == 'Training') {
          await duatangan
            .get('trainings/fetchNewTraining')
            .then((response) => {
              if (!mountedRef.current) return null;
              // setListData(response.data);
              setListData(Array.isArray(response.data) ? response.data : [])
              console.log(response);
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });
        }
      } catch (error) {
        console.error(error);
      }
    };

    if (listData == null) refreshData();
    return () => {
      mountedRef.current = false;
    };
  }, []);

  keyExtractor = (item) => {
    return props.type == 'News'
      ? item.id.toString()
      : item.id_training.toString();
  };

  renderItem = ({item}) => {
    if (props.type == 'News')
      return (
        <View style={styles.itemContainer}>
          <Item
            id={item.id.toString()}
            background={item._embedded['wp:featuredmedia']['0'].source_url}
            date={Moment(item.date).format('DD MMM YYYY')}
            title={item.title.rendered}
            type={props.type}
            navigation={props.navigation}
          />
        </View>
      );

    if (props.type == 'Training')
      return (
        <View style={styles.itemContainer}>
          <Item
            id={item.id_training.toString()}
            background={item.banner}
            date={Moment(item.mulai).format('DD MMM YYYY')}
            title={item.nama}
            type={props.type}
            navigation={props.navigation}
          />
        </View>
      );

    return null;
  };

  footerItem = () => (
    <View>
      <TouchableOpacity onPress={props.onPress}>
        <View style={styles.nextContainer}>
          <View>
            <FAIcon name={'chevron-circle-right'} style={styles.nextIcon} />
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );

  const emptyListMessage = () => {
      return (
          <Text style={{textAlign: 'center'}}>Training Belum Tersedia!</Text>
      );
  };

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity style={styles.titleContainer} onPress={props.onPress}>
        <Text style={styles.titleText}>{props.title}</Text>
        <FAIcon style={styles.titleIcon} name={'chevron-circle-right'} />
      </TouchableOpacity>
      {listData == null ? (
        <ActivityIndicator />
      ) : (
        <View style={styles.listContainer}>
          <FlatList
            data={listData}
            horizontal={true}
            keyExtractor={keyExtractor}
            renderItem={renderItem}
            // ListFooterComponent={footerItem}
            ListEmptyComponent={emptyListMessage}
          />
        </View>
      )}
    </SafeAreaView>
  );
};

export default Feeds;
export {Item, List, Detail};
