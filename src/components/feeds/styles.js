import {StyleSheet, Dimensions} from 'react-native';
const {width: WIDTH} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },

  // Title
  titleContainer: {
    width: WIDTH - 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'bold',
  },
  titleIcon: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'bold',
  },

  // List
  listContainer: {
    width: WIDTH - 20,
    marginTop: 10,
    marginBottom: 10,
    height: 200,
    borderRadius: 15,
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  itemContainer: {
    width: WIDTH - 70,
    height: 200,
    marginRight: 10,
    flex: 1,
    flexDirection: 'column',
  },

  // Image
  imageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    borderRadius: 15,
    width: '100%',
    height: '100%',
  },

  // Time
  timeContainer: {
    position: 'absolute',
    left: 0,
    bottom: 50,
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    padding: 5,
  },
  time: {
    color: '#FFFFFF',
  },

  bottomContainer: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    height: 50,
    width: '100%',
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'rgba(250, 245, 230, 0.8)',

    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  // Subtitle & Button
  subtitleContainer: {
    flex: 3,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  subtitleText: {
    color: '#000000',
    fontSize: 14,
    fontWeight: 'bold',
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonStyle: {
    width: '100%',
    height: '70%',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#D3A40E',
    backgroundColor: 'rgba(211, 164, 14, 0.8)',
    borderRadius: 10,
  },
  buttonText: {
    color: '#faf5e6',
    fontWeight: 'bold',
  },

  // Next
  nextContainer: {
    width: 60,
    height: 190,
    borderRadius: 15,
    backgroundColor: '#D3A40E',
    alignItems: 'center',
    justifyContent: 'center',
  },
  nextIcon: {
    color: '#faf5e6',
    fontSize: 16,
    fontWeight: 'bold',
  },

  // Card List
  cardContainer: {
    width: WIDTH - 30,
    height: 200,
    borderRadius: 15,
    justifyContent: 'flex-start',
    marginVertical: 7,
    backgroundColor: 'white',
  },
  cardImage: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flex: 1,
    height: 200,
    width: null,
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  // Detail Containter
  detailContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#faf5e6',
    paddingTop: 10,
  },
  detailTitle: {
    marginTop: 10,
  },
  imageHtml: {
    width: '100%',
    width: WIDTH - 40,
    borderRadius: 10,
    height: 200,
    margin: 20,
  },
  htmlContent: {
    flex: 1,
    width: '100%',
    backgroundColor: '#faf5e6',
    padding: 10,
  },
  html: {
    backgroundColor: 'transparent',
  },
});

export default styles;
