import {StyleSheet} from 'react-native';
import {Dimensions} from 'react-native';

const {width: WIDTH} = Dimensions.get('screen');

const loginStyles = StyleSheet.create({
  logo: {
    width: 120,
    height: 120,
    opacity: 0.7,
    alignItems: 'center',
    marginBottom: 50,
  },
});

const styles = StyleSheet.create({
  imageBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  title: {
    top: 15,
    fontWeight: 'bold',
    fontSize: 18,
    position: 'absolute',
  },
  version: {
    fontSize: 10,
    position: 'absolute',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  gold12: {
    color: '#D3A40E',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

const input = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  icon: {
    position: 'absolute',
    top: 12,
    left: 40,
    fontSize: 20,
    color: '#636363',
  },
  text: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    fontWeight: 'bold',
    paddingLeft: 45,
    color: '#636363',
    backgroundColor: 'rgba(211, 165, 13, 0.35)',
    marginHorizontal: 25,
  },
  placeholder: {
    color: '#636363',
  },
});

const buttons = StyleSheet.create({
  primary: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#D3A40E',
    justifyContent: 'center',
    marginTop: 20,
    marginHorizontal: 25,
  },
  textPrimary: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  eye: {
    position: 'absolute',
    top: 12,
    right: 40,
  },
  eyeIcon: {
    fontSize: 20,
    color: '#636363',
  },
});

export {loginStyles, styles, buttons, input};
