import React from 'react';
import {
  View,
  SafeAreaView,
  Dimensions,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
} from 'react-native';
import Animated from 'react-native-reanimated';
import {useFocusEffect} from '@react-navigation/native';
import styles from './styles';

// Components
import AuthContext from '../../AuthContext';
import NumberFormat from 'react-number-format';
import {ScrollView} from 'react-native-gesture-handler';
import {TabView} from 'react-native-tab-view';

// Services
import server from '../../services/server';

const PayslipItem = ({data}) => {
  const [totalAllowance, setTotalAllowance] = React.useState(0);
  const [totalDeduction, setTotalDeduction] = React.useState(0);
  const [allowanceVisible, setAllowanceVisible] = React.useState(false);
  const [deductionVisible, setDeductionVisible] = React.useState(false);

  useFocusEffect(
    React.useCallback(() => {
      if (data != null) {
        setTotalAllowance(
          parseFloat(data?.allowance) + parseFloat(data?.rounding),
        );

        setTotalDeduction(
          parseFloat(data?.deduction) +
            parseFloat(data?.loan) +
            parseFloat(data?.saving),
        );
      }
    }, [data]),
  );

  return (
    <ScrollView>
      {data == null ? (
        <View style={styles.detailContainer}>
          <Text>No Data</Text>
        </View>
      ) : (
        <View style={styles.detailContainer}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={allowanceVisible}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View style={{alignItems: 'flex-start', marginBottom: 10}}>
                  {data.detailAllowance.map((item, i) => {
                    return (
                      <View key={i} style={styles.modalRow}>
                        <View style={styles.modalItem}>
                          <Text>{item.name}</Text>
                        </View>
                        <NumberFormat
                          value={item.amount}
                          displayType={'text'}
                          thousandSeparator={true}
                          decimalScale={0}
                          renderText={(value) => (
                            <Text style={styles.valueText}>{value}</Text>
                          )}
                        />
                      </View>
                    );
                  })}
                  <View
                    style={[
                      styles.modalRow,
                      {borderTopWidth: 1, borderColor: '#D3A40E'},
                    ]}>
                    <View style={styles.modalItem}>
                      <Text>Total Allowance</Text>
                    </View>
                    <NumberFormat
                      value={data.allowance}
                      displayType={'text'}
                      thousandSeparator={true}
                      decimalScale={0}
                      renderText={(value) => (
                        <Text style={styles.valueText}>{value}</Text>
                      )}
                    />
                  </View>
                </View>

                <TouchableOpacity
                  style={styles.closeButton}
                  onPress={() => {
                    setAllowanceVisible(!allowanceVisible);
                  }}>
                  <Text style={styles.textStyle}>Close</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>

          <Modal
            animationType="slide"
            transparent={true}
            visible={deductionVisible}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View style={{alignItems: 'flex-start', marginBottom: 10}}>
                  {data.detailDeduction.map((item, i) => {
                    return (
                      <View key={i} style={styles.modalRow}>
                        <View style={styles.modalItem}>
                          <Text>{item.name}</Text>
                        </View>
                        <NumberFormat
                          value={item.amount}
                          displayType={'text'}
                          thousandSeparator={true}
                          decimalScale={0}
                          renderText={(value) => (
                            <Text style={styles.valueText}>{value}</Text>
                          )}
                        />
                      </View>
                    );
                  })}
                  <View
                    style={[
                      styles.modalRow,
                      {borderTopWidth: 1, borderColor: '#D3A40E'},
                    ]}>
                    <View style={styles.modalItem}>
                      <Text>Total Deduction</Text>
                    </View>
                    <NumberFormat
                      value={data.deduction}
                      displayType={'text'}
                      thousandSeparator={true}
                      decimalScale={0}
                      renderText={(value) => (
                        <Text style={styles.valueText}>{value}</Text>
                      )}
                    />
                  </View>
                </View>

                <TouchableOpacity
                  style={styles.closeButton}
                  onPress={() => {
                    setDeductionVisible(!deductionVisible);
                  }}>
                  <Text style={styles.textStyle}>Close</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>

          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={[styles.groupContainer, {flex: 1}]}>
              <View style={{padding: 10}}>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>HKS</Text>
                <Text style={{alignSelf: 'flex-end'}}>{data.hks}</Text>
              </View>
            </View>
            <View style={[styles.groupContainer, {flex: 1}]}>
              <View style={{padding: 10}}>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>HKA</Text>
                <Text style={{alignSelf: 'flex-end'}}>{data.hka}</Text>
              </View>
            </View>
            <View style={[styles.groupContainer, {flex: 1}]}>
              <View style={{padding: 10}}>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>LATE</Text>
                <Text style={{alignSelf: 'flex-end'}}>{data.late}</Text>
              </View>
            </View>
          </View>

          <View
            style={[
              styles.groupContainer,
              {paddingTop: 10, paddingBottom: 10, backgroundColor: '#dbb63e'},
            ]}>
            <View style={styles.itemContainer}>
              <Text style={styles.dataLabel}>Basic Salary</Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={data.salary}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <Text style={styles.valueText}>{value}</Text>
                  )}
                />
              </View>
            </View>
          </View>

          <View style={styles.groupContainer}>
            <View style={styles.itemContainer}>
              <Text style={styles.dataLabel}>Allowance</Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={data.allowance}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <TouchableOpacity
                      onPress={() => {
                        setAllowanceVisible(true);
                      }}>
                      <Text style={styles.valueText}>{value}</Text>
                    </TouchableOpacity>
                  )}
                />
              </View>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.dataLabel}>Rounding</Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={data.rounding}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <Text style={styles.valueText}>{value}</Text>
                  )}
                />
              </View>
            </View>
            <View style={[styles.itemContainer, styles.sumText]}>
              <Text style={styles.dataLabel}>Total Allowance</Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={totalAllowance}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <Text style={styles.valueText}>{value}</Text>
                  )}
                />
              </View>
            </View>
          </View>

          <View style={styles.groupContainer}>
            <View style={styles.itemContainer}>
              <Text style={styles.dataLabel}>Deduction</Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={data.deduction}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <TouchableOpacity
                      onPress={() => {
                        setDeductionVisible(true);
                      }}>
                      <Text style={styles.valueText}>{value}</Text>
                    </TouchableOpacity>
                  )}
                />
              </View>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.dataLabel}>Loan</Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={data.loan}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <Text style={styles.valueText}>{value}</Text>
                  )}
                />
              </View>
            </View>
            <View style={styles.itemContainer}>
              <Text style={styles.dataLabel}>Saving</Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={data.saving}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <Text style={styles.valueText}>{value}</Text>
                  )}
                />
              </View>
            </View>
            <View style={[styles.itemContainer, styles.sumText]}>
              <Text style={styles.dataLabel}>Total Deduction</Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={totalDeduction}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <Text style={styles.valueText}>{value}</Text>
                  )}
                />
              </View>
            </View>
          </View>

          <View
            style={[
              styles.groupContainer,
              {paddingTop: 10, paddingBottom: 10, backgroundColor: '#dbb63e'},
            ]}>
            <View style={styles.itemContainer}>
              <Text style={[styles.dataLabel, {fontWeight: 'bold'}]}>
                Take Home Pay
              </Text>
              <View style={styles.valueContainer}>
                <NumberFormat
                  value={data.netto}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={0}
                  renderText={(value) => (
                    <Text style={[styles.valueText, {fontWeight: 'bold'}]}>
                      {value}
                    </Text>
                  )}
                />
              </View>
            </View>
          </View>
        </View>
      )}
    </ScrollView>
  );
};

const PayslipTab = () => {
  const {state} = React.useContext(AuthContext);

  const [data, setData] = React.useState(null);
  const [tab1, setTab1] = React.useState(null);
  const [tab2, setTab2] = React.useState(null);
  const [tab3, setTab3] = React.useState(null);

  const [index, setIndex] = React.useState(2);
  const [routes] = React.useState([
    {key: 'first'},
    {key: 'second'},
    {key: 'third'},
  ]);

  React.useEffect(() => {
    if (state == null || state.employee == null) {
      setData(null);
      setTab1(null);
      setTab2(null);
      setTab3(null);
      setIndex(2);
      return;
    }

    server
      .get('salaries/latest')
      .then((response) => {
        setData(response.data);
        setIndex(2);

        response.data.data.map((salary, i) => {
          if (i == 0) setTab3(salary);
          if (i == 1) setTab2(salary);
          if (i == 2) setTab1(salary);
        });
      })
      .catch((error) => {
        alert(error);
        console.log(error);
      });
  }, [state]);

  useFocusEffect(
    React.useCallback(() => {
      if (state == null || state.employee == null) {
        setData(null);
        setTab1(null);
        setTab2(null);
        setTab3(null);
        setIndex(2);
        return;
      }

      server
        .get('salaries/latest')
        .then((response) => {
          setData(response.data);
          setIndex(2);

          response.data.data.map((salary, i) => {
            if (i == 0) setTab3(salary);
            if (i == 1) setTab2(salary);
            if (i == 2) setTab1(salary);
          });
        })
        .catch((error) => {
          alert(error);
          console.log(error);
        });
    }, [state]),
  );

  renderTabBar = (props) => {
    if (data == null) return null;

    const inputRange = routes.map((x, i) => i);
    return (
      <View style={styles.tabBar}>
        {routes.map((route, i) => {
          const monthNames = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Juy',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
          ];
          let data = i == 0 ? tab1 : i == 1 ? tab2 : tab3;
          let title = 'No Data';
          if (data != null)
            title = `${monthNames[data.month - 1]} ${data.year}`;

          const color = Animated.color(
            Animated.round(
              Animated.interpolate(props.position, {
                inputRange,
                outputRange: inputRange.map((inputIndex) =>
                  inputIndex === i ? 211 : 177,
                ),
              }),
            ),
            Animated.round(
              Animated.interpolate(props.position, {
                inputRange,
                outputRange: inputRange.map((inputIndex) =>
                  inputIndex === i ? 164 : 177,
                ),
              }),
            ),
            Animated.round(
              Animated.interpolate(props.position, {
                inputRange,
                outputRange: inputRange.map((inputIndex) =>
                  inputIndex === i ? 14 : 177,
                ),
              }),
            ),
          );

          return (
            <TouchableOpacity
              key={route.key}
              style={styles.tabItem}
              onPress={() => setIndex(i)}>
              <Animated.View
                style={[styles.tabItemContainer, {borderColor: color}]}>
                <Animated.Text style={[styles.tabItemText, {color}]}>
                  {title}
                </Animated.Text>
              </Animated.View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  renderScene = ({route}) => {
    switch (route.key) {
      case 'first':
        return <PayslipItem data={tab1} />;
      case 'second':
        return <PayslipItem data={tab2} />;
      case 'third':
        return <PayslipItem data={tab3} />;
    }
  };

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: Dimensions.get('window').width}}
      renderTabBar={renderTabBar}
      style={styles.container}
    />
  );
};

function payslip({navigation}) {
  const {state} = React.useContext(AuthContext);

  React.useEffect(() => {
    if (state == null || state.employee == null) {
      navigation.navigate('Login');
      return;
    }

    if (state != null && state.lessSecure) {
      navigation.navigate('Change Password');
      return;
    }
  }, [state]);

  useFocusEffect(
    React.useCallback(() => {
      if (state == null || state.employee == null) {
        navigation.navigate('Login');
        return;
      }

      if (state != null && state.lessSecure) {
        navigation.navigate('Change Password');
        return;
      }
    }, [state]),
  );

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.container}>
        <View style={styles.employeeContainer}>
          {state == null || state.employee == null ? (
            <ActivityIndicator size="large" color="gold" />
          ) : (
            <View>
              <Text numberOfLines={1} style={styles.title}>
                {state.employee.name}
              </Text>
              <Text numberOfLines={1} style={styles.subtitle}>
                {state.employee.company}
              </Text>
              <Text numberOfLines={1} style={styles.subtitle}>
                {state.employee.position}
              </Text>
            </View>
          )}
        </View>
        <PayslipTab />
      </SafeAreaView>
    </View>
  );
}

export default payslip;
