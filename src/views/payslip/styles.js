import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#faf5e6',
  },
  employeeContainer: {
    padding: 10,
    marginTop: 30,
    width: '100%',
  },
  title: {
    alignSelf: 'center',
    fontSize: 22,
    color: '#000000',
    fontWeight: '600',
  },
  subtitle: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#000000',
    fontWeight: '400',
  },

  tabBar: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  tabItemContainer: {
    alignItems: 'center',
    width: '100%',
    paddingBottom: 5,
    borderBottomWidth: 3,
  },
  tabItemText: {
    fontWeight: 'bold',
    fontSize: 16,
  },

  detailContainer: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },

  groupContainer: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    borderColor: '#D3A40E',
    borderRadius: 15,
    borderWidth: 1,
  },

  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
  },
  sumText: {
    borderColor: '#D3A40E',
    borderTopWidth: 1,
    marginTop: 5,
    paddingBottom: 7,
  },

  dataContainer: {
    flexDirection: 'row',
    width: '90%',
  },
  dataLabel: {
    fontSize: 14,
    alignSelf: 'flex-start',
    width: '50%',
    paddingLeft: 10,
  },
  valueContainer: {
    alignItems: 'flex-end',
    paddingRight: 10,
    width: '50%',
  },
  valueText: {
    fontSize: 14,
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    margin: 10,
    backgroundColor: '#faf5e6',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  closeButton: {
    backgroundColor: '#D3A40E',
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalRow: {
    flexDirection: 'row',
    paddingBottom: 5,
  },
  modalItem: {
    flex: 1,
    alignSelf: 'flex-start',
  },
  modalLabel: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

//   tabBar: {
//     flexDirection: 'row',
//   },
//   tabItem: {
//     flex: 1,
//     alignItems: 'center',
//     padding: 16,
//   },

//   noLabel: {
//     display: 'none',
//     height: 0,
//   },
//   bubble: {
//     backgroundColor: 'lime',
//     paddingHorizontal: 18,
//     paddingVertical: 12,
//     borderRadius: 10,
//   },

export default styles;
