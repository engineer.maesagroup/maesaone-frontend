import React from 'react';
import {SafeAreaView, View, Text, Dimensions, FlatList} from 'react-native';
import styles from './styles';
import {useFocusEffect} from '@react-navigation/native';

// Components
import AuthContext from '../../AuthContext';
import {TabView} from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import DatePicker from 'react-native-date-ranges';
import Moment from 'moment';
import FAIcon from 'react-native-vector-icons/FontAwesome';

// Services
import server from '../../services/server';
import {TouchableOpacity} from 'react-native-gesture-handler';

const TabScreen = ({renderItem, data}) => {
  return (
    <View style={styles.detailContainer}>
      {data == null || data.length == 0 ? (
        <View style={[styles.listContainer, {flex: 1, alignItems: 'center'}]}>
          <Text>No Data</Text>
        </View>
      ) : (
        <View style={styles.listContainer}>
          <FlatList
            data={data}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderItem}
          />
        </View>
      )}
    </View>
  );
};

const HistoryTab = ({startDate, endDate}) => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'clockIn', title: 'Clock In'},
    {key: 'overtime', title: 'Overtime'},
    {key: 'absent', title: 'Absent'},
    {key: 'training', title: 'Training'},
  ]);

  const [dataAttendance, setDataAttendance] = React.useState(null);
  const [dataOvertime, setDataOvertime] = React.useState(null);
  const [dataUnattendance, setDataUnattendance] = React.useState(null);
  const [dataTraining, setDataTraining] = React.useState(null);

  useFocusEffect(
    React.useCallback(() => {
      const refreshData = async () => {
        if (startDate != null && endDate != null) {
          await server
            .post('attendances/filter', {startDate, endDate})
            .then((response) => {
              setDataAttendance(response.data.data);
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });

          await server
            .post('overtimes/filter', {startDate, endDate})
            .then((response) => {
              setDataOvertime(response.data.data);
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });

          await server
            .post('unattendances/filter', {startDate, endDate})
            .then((response) => {
              setDataUnattendance(response.data.data);
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });

          await server
            .post('trainings/filter', {startDate, endDate})
            .then((response) => {
              setDataTraining(response.data.data);
            })
            .catch((error) => {
              alert(error);
              console.log(error);
            });
        }
      };

      refreshData();
    }, [startDate, endDate]),
  );

  renderScene = ({route}) => {
    switch (route.key) {
      case 'clockIn':
        return (
          <TabScreen data={dataAttendance} renderItem={renderAttendance} />
        );
      case 'overtime':
        return <TabScreen data={dataOvertime} renderItem={renderOvertime} />;
      case 'absent':
        return (
          <TabScreen data={dataUnattendance} renderItem={renderUnattendance} />
        );
      case 'training':
        return <TabScreen data={dataTraining} renderItem={renderTraining} />;
    }
  };

  renderAttendance = ({item}) => {
    return (
      <View style={styles.itemContainer}>
        <View style={styles.dateContainer}>
          <View style={styles.dateLeft}>
            <Text style={styles.dateTitle}>
              {Moment(item.transdate).format('DD')}
            </Text>
          </View>
          <View style={styles.dateRight}>
            <Text style={styles.dateSubtitle}>
              {Moment(item.transdate).format('MMM')}
            </Text>
            <Text style={styles.dateSubtitle}>
              {Moment(item.transdate).format('yyyy')}
            </Text>
          </View>
        </View>
        <View style={styles.descContainer}>
          <View
            style={[
              styles.subitemContainer,
              {borderRightWidth: 1, borderColor: '#D3A40E', flex: 1},
            ]}>
            <Text style={styles.descText}>{item.attendance_type}</Text>
          </View>
          <View style={[styles.subitemContainer, {flex: 1}]}>
            <Text style={styles.descText}>{item.check_time}</Text>
          </View>
        </View>
      </View>
    );
  };

  renderOvertime = ({item}) => {
    return (
      <View style={styles.itemContainer}>
        <View style={styles.dateContainer}>
          <View style={styles.dateLeft}>
            <Text style={styles.dateTitle}>
              {Moment(item.transdate).format('DD')}
            </Text>
          </View>
          <View style={styles.dateRight}>
            <Text style={styles.dateSubtitle}>
              {Moment(item.transdate).format('MMM')}
            </Text>
            <Text style={styles.dateSubtitle}>
              {Moment(item.transdate).format('yyyy')}
            </Text>
          </View>
        </View>
        <View style={styles.descContainer}>
          <View
            style={[
              styles.subitemContainer,
              {alignItems: 'flex-start', flex: 4},
            ]}>
            <Text numberOfLines={3} style={styles.descSmallText}>
              {item.reason}
            </Text>
          </View>
          <View style={[styles.subitemContainer, {flex: 1}]}>
            {item.approval_1 == 1 ? (
              <FAIcon
                name="check-circle"
                size={24}
                style={{color: '#196F3D'}}
              />
            ) : item.approval_1 == 2 ? (
              <FAIcon
                name="times-circle"
                size={24}
                style={{color: '#bf151e'}}
              />
            ) : (
              <FAIcon name="circle-o" size={24} style={{color: '#d1d1d1'}} />
            )}
          </View>
          <View style={[styles.subitemContainer, {flex: 1}]}>
            {item.approval_2 == 1 ? (
              <FAIcon
                name="check-circle"
                size={24}
                style={{color: '#196F3D'}}
              />
            ) : item.approval_2 == 2 ? (
              <FAIcon
                name="times-circle"
                size={24}
                style={{color: '#bf151e'}}
              />
            ) : (
              <FAIcon name="circle-o" size={24} style={{color: '#d1d1d1'}} />
            )}
          </View>
        </View>
      </View>
    );
  };

  renderUnattendance = ({item}) => {
    return (
      <View style={styles.itemContainer}>
        <View style={styles.dateContainer}>
          <View style={styles.dateLeft}>
            <Text style={styles.dateTitle}>
              {Moment(item.transdate).format('DD')}
            </Text>
          </View>
          <View style={styles.dateRight}>
            <Text style={styles.dateSubtitle}>
              {Moment(item.transdate).format('MMM')}
            </Text>
            <Text style={styles.dateSubtitle}>
              {Moment(item.transdate).format('yyyy')}
            </Text>
          </View>
        </View>
        <View style={styles.descContainer}>
          <View
            style={[
              styles.subitemContainer,
              {alignItems: 'flex-start', flex: 4},
            ]}>
            <Text numberOfLines={3} style={styles.descSmallText}>
              {item.reason}
            </Text>
          </View>
          <View style={[styles.subitemContainer, {flex: 1}]}>
            {item.approval_1 == 1 ? (
              <FAIcon
                name="check-circle"
                size={24}
                style={{color: '#196F3D'}}
              />
            ) : item.approval_1 == 2 ? (
              <FAIcon
                name="times-circle"
                size={24}
                style={{color: '#bf151e'}}
              />
            ) : (
              <FAIcon name="circle-o" size={24} style={{color: '#d1d1d1'}} />
            )}
          </View>
          <View style={[styles.subitemContainer, {flex: 1}]}>
            {item.approval_2 == 1 ? (
              <FAIcon
                name="check-circle"
                size={24}
                style={{color: '#196F3D'}}
              />
            ) : item.approval_2 == 2 ? (
              <FAIcon
                name="times-circle"
                size={24}
                style={{color: '#bf151e'}}
              />
            ) : (
              <FAIcon name="circle-o" size={24} style={{color: '#d1d1d1'}} />
            )}
          </View>
        </View>
      </View>
    );
  };

  renderTraining = ({item}) => {
    return (
      <View style={styles.itemContainer}>
        <View style={styles.dateContainer}>
          <View style={styles.dateLeft}>
            <Text style={styles.dateTitle}>
              {Moment(item.transdate).format('DD')}
            </Text>
          </View>
          <View style={styles.dateRight}>
            <Text style={styles.dateSubtitle}>
              {Moment(item.transdate).format('MMM')}
            </Text>
            <Text style={styles.dateSubtitle}>
              {Moment(item.transdate).format('yyyy')}
            </Text>
          </View>
        </View>
        <View style={styles.descContainer}>
          <View style={[styles.subitemContainer, {alignItems: 'flex-start'}]}>
            <Text numberOfLines={3} style={styles.descSmallText}>
              {item.title}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  renderTabBar = (props) => {
    const inputRange = routes.map((x, i) => i);

    return (
      <View style={styles.tabBar}>
        {routes.map((route, i) => {
          const title = route.title;

          const backgroundColor = Animated.interpolateColors(props.position, {
            inputRange,
            outputColorRange: inputRange.map((inputIndex) =>
              inputIndex === i ? '#f6ecce' : '#d3a40e',
            ),
          });

          const color = Animated.interpolateColors(props.position, {
            inputRange,
            outputColorRange: inputRange.map((inputIndex) =>
              inputIndex === i ? '#636363' : '#faf5e6',
            ),
          });

          return (
            <TouchableOpacity
              key={route.key}
              style={styles.tabItem}
              onPress={() => setIndex(i)}>
              <Animated.View
                style={[
                  styles.tabItemContainer,
                  {backgroundColor: backgroundColor},
                ]}>
                <Animated.Text style={[styles.tabItemText, {color: color}]}>
                  {title}
                </Animated.Text>
              </Animated.View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={setIndex}
      initialLayout={{width: Dimensions.get('window').width}}
      style={styles.tabContainer}
    />
  );
};

function history({navigation}) {
  const {state} = React.useContext(AuthContext);

  const [startDate, setStartDate] = React.useState(null);
  const [endDate, setEndDate] = React.useState(null);

  React.useEffect(() => {
    if (state == null || state.employee == null) {
      navigation.navigate('Login');
      return;
    }

    if (state != null && state.lessSecure) {
      navigation.navigate('Change Password');
      return;
    }
  }, [state]);

  useFocusEffect(
    React.useCallback(() => {
      if (state == null || state.employee == null) {
        navigation.navigate('Login');
        return;
      }

      if (state != null && state.lessSecure) {
        navigation.navigate('Change Password');
        return;
      }
    }, [state]),
  );

  handleOnConfirm = ({startDate, endDate}) => {
    setStartDate(startDate);
    setEndDate(endDate);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.title}>Personal History Data</Text>
        <DatePicker
          style={styles.datePicker}
          customStyles={{
            placeholderText: styles.placeholderText,
            contentText: styles.contentText,
            headerStyle: styles.headerStyle,
            headerMarkTitle: {fontSize: 16, color: '#ffffff'},
            headerDateTitle: {fontSize: 16, color: '#ffffff'},
          }}
          centerAlign
          allowFontScaling={false}
          selectedBgColor="#D3A40E"
          placeholder={'Please select date range'}
          mode={'range'}
          markText="Please select date range"
          ButtonStyle={styles.buttonStyle}
          ButtonTextStyle={styles.buttonTextStyle}
          ButtonText="Apply"
          onConfirm={handleOnConfirm}
        />
      </View>
      <HistoryTab startDate={startDate} endDate={endDate} />
    </SafeAreaView>
  );
}

export default history;
