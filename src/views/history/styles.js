import {StyleSheet, Dimensions} from 'react-native';
const {width: WIDTH} = Dimensions.get('screen');
const tabWidth = (WIDTH - 40) / 4;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D3A40E',
  },
  headerContainer: {
    padding: 10,
    marginTop: 10,
    width: '100%',
  },
  title: {
    alignSelf: 'center',
    fontSize: 22,
    color: '#faf5e6',
    fontWeight: '600',
  },

  datePicker: {
    marginTop: 10,
    borderWidth: 0,
    borderRadius: 25,
    backgroundColor: '#faf5e6',
    height: 38,
  },
  headerStyle: {
    backgroundColor: '#D3A40E',
    fontSize: 10,
    paddingTop: 60,
    height: 120,
  },
  placeholderText: {
    fontSize: 16,
    color: '#D3A40E',
  },
  contentText: {
    fontSize: 16,
    color: '#D3A40E',
  },
  buttonStyle: {
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    backgroundColor: '#faf5e6',
    borderColor: '#D3A40E',
  },
  buttonTextStyle: {
    color: '#D3A40E',
    fontSize: 16,
    fontWeight: 'bold',
  },

  tabContainer: {
    flex: 1,
    width: '100%',
  },

  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    width: tabWidth,
    margin: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabItemContainer: {
    alignItems: 'center',
    width: '100%',
    padding: 5,
    borderRadius: 15,
    borderWidth: 0,
    borderColor: '#faf5e6',
  },
  tabItemText: {
    fontSize: 14,
  },

  detailContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#faf5e6',
  },

  listContainer: {
    justifyContent: 'center',
    marginTop: 10,
    width: '100%',
  },

  itemContainer: {
    padding: 5,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
  },
  dateContainer: {
    borderRadius: 10,
    padding: 10,
    flex: 3,
    backgroundColor: '#D3A40E',
    alignItems: 'center',
    flexDirection: 'row',
  },
  dateLeft: {
    flex: 1,
    alignItems: 'center',
  },
  dateRight: {
    flex: 1,
    alignItems: 'center',
  },
  dateTitle: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  dateSubtitle: {
    fontSize: 18,
  },

  descContainer: {
    borderRadius: 10,
    marginLeft: 10,
    padding: 10,
    flex: 7,
    borderWidth: 1,
    borderColor: '#D3A40E',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  subitemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  descText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  descText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default styles;
