import {StyleSheet, Dimensions} from 'react-native';
const {width: WIDTH} = Dimensions.get('screen');
const buttonWidth = (WIDTH - 65) / 2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 120,
    height: 120,
    opacity: 0.7,
    alignItems: 'center',
    marginBottom: 50,
  },
  inputContainer: {
    marginTop: 10,
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    marginHorizontal: 25,
  },
  textContainer: {
    backgroundColor: 'rgba(211, 165, 13, 0.35)',
  },
  inputIcon: {
    position: 'absolute',
    top: 12,
    left: 40,
    fontSize: 20,
    color: '#919191',
  },
  inputText: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingTop: 12,
    paddingLeft: 65,
    paddingRight: 65,
    color: '#636363',
  },
  eyeContainer: {
    position: 'absolute',
    top: 12,
    right: 40,
  },
  eyeIcon: {
    fontSize: 20,
    color: '#919191',
  },

  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },

  buttonStyle: {
    backgroundColor: '#D3A40E',
    justifyContent: 'center',
    width: buttonWidth,
    borderRadius: 25,
  },
  buttonIcon: {
    position: 'absolute',
    top: 12,
    left: 40,
    fontSize: 20,
    color: '#FFFFFF',
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
});

export default styles;
