import React from 'react';
import {
  ImageBackground,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Keyboard,
  ActivityIndicator,
} from 'react-native';
import styles from './style';

// Components
import backgroundImage from '../../assets/images/main.jpg';
import logo from '../../assets/icons/logo.png';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import AuthContext from '../../AuthContext';

// Services
import server from '../../services/server';

function login({navigation}) {
  const {dispatch} = React.useContext(AuthContext);

  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [hidePassword, setHidePassword] = React.useState(true);
  const [loading, setLoading] = React.useState(false);

  handleSignIn = async () => {
    Keyboard.dismiss();
    setLoading(true);

    if (username == '') {
      alert('Please fill input nik');
      setLoading(false);
      return;
    }

    if (password == '') {
      alert('Please fill input password');
      setLoading(false);
      return;
    }

    await server
      .post('login', {username, password})
      .then((response) => {
        setPassword('');
        setLoading(false);
        let token = response.data.data.token;

        server.defaults.headers.common = {Authorization: `Bearer ${token}`};

        // Get Employee Detail
        server
          .get('employees/me')
          .then((response) => {
            let employee = response.data.data.data;
            let lessSecure = response.data.data.lessSecure;
            let task = response.data.data.task;
            dispatch({
              type: 'SIGN_IN',
              token: token,
              employee: employee,
              lessSecure: lessSecure,
              task: task,
            });
            navigation.goBack();
          })
          .catch((error) => {
            console.log(error.response);
          });
      })
      .catch((error) => {
        setPassword('');
        setLoading(false);
        console.log(error.response.data.error);
        alert(
          'Login failed. Please try again (' + error.response.data.error + ')',
        );
      });
  };

  handleEye = () => {
    if (hidePassword) {
      setHidePassword(false);
    } else {
      setHidePassword(true);
    }
  };

  buttonOrLoading = () => {
    if (loading) {
      return (
        <View style={[styles.buttonStyle, {marginLeft: 5}]}>
          <ActivityIndicator size="small" color="gold" />
        </View>
      );
    }
    return (
      <TouchableOpacity
        style={[styles.buttonStyle, {marginLeft: 5}]}
        onPress={this.handleSignIn}>
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>
    );
  };

  return (
    <ImageBackground source={backgroundImage} style={styles.container}>
      <Image source={logo} style={styles.logo} />
      <View style={[styles.inputContainer, styles.textContainer]}>
        <FAIcon style={styles.inputIcon} name={'user'} />
        <TextInput
          style={styles.inputText}
          placeholder={'NIK'}
          returnKeyType={'next'}
          autoCorrect={false}
          placeholderTextColor={'#b1b1b1'}
          underlineColorAndroid={'transparent'}
          value={username}
          onChangeText={(text) => setUsername(text)}
        />
      </View>
      <View style={[styles.inputContainer, styles.textContainer]}>
        <FAIcon style={styles.inputIcon} name={'lock'} />
        <TextInput
          style={styles.inputText}
          placeholder={'Password'}
          autoCorrect={false}
          placeholderTextColor={'#b1b1b1'}
          underlineColorAndroid={'transparent'}
          value={password}
          onChangeText={(text) => setPassword(text)}
          secureTextEntry={hidePassword}
        />
        <TouchableOpacity style={styles.eyeContainer} onPress={this.handleEye}>
          <FAIcon
            style={styles.eyeIcon}
            name={hidePassword ? 'eye-slash' : 'eye'}
          />
        </TouchableOpacity>
      </View>
      <View style={[styles.inputContainer, styles.buttonContainer]}>
        <TouchableOpacity
          style={[styles.buttonStyle, {marginRight: 5}]}
          onPress={() => navigation.push('Home')}>
          <Text style={styles.buttonText}>
            <FAIcon style={styles.buttonIcon} name={'arrow-circle-left'} />
            {'  '}
            Home
          </Text>
        </TouchableOpacity>
        {buttonOrLoading()}
      </View>
    </ImageBackground>
  );
}

export default login;
