import React from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
  TextInput,
  ActivityIndicator,
  Button,
} from 'react-native';
import styles from './styles';

// Components
import AuthContext from '../../AuthContext';
import Moment from 'moment';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

// Services
import server from '../../services/server';

const overtime = () => {
  const {state} = React.useContext(AuthContext);

  const [name, setName] = React.useState('Guest');
  const [company, setCompany] = React.useState('Please Sign In');
  const [photo, setPhoto] = React.useState({
    uri: 'https://www.maesaone.com/assets/images/user.png',
  });

  const [transdate, setTransDate] = React.useState(new Date());
  const [startHour, setStartHour] = React.useState(new Date());
  const [endHour, setEndHour] = React.useState(new Date());
  const [reason, setReason] = React.useState('');

  const [showDate, setShowDate] = React.useState(false);
  const [showStartHour, setShowStartHour] = React.useState(false);
  const [showEndHour, setShowEndHour] = React.useState(false);

  const [data, setData] = React.useState(null);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (state != null && state.employee != null) {
      setName(state.employee.name);
      setCompany(state.employee.company);
      if (state.employee.photo != null)
        setPhoto({uri: `https://www.maesaone.com/${state.employee.photo}`});
      else setPhoto({uri: 'https://www.maesaone.com/assets/images/user.png'});
    } else {
      setName('Guest');
      setCompany('Please Sign In');
      setPhoto({uri: 'https://www.maesaone.com/assets/images/user.png'});
    }

    const refreshData = async () => {
      await server
        .post('overtimes/unapproved')
        .then((response) => {
          setData(response.data.data);
        })
        .catch((error) => {
          alert(error);
          console.log(error);
        });
    };

    refreshData();
  }, [state]);

  renderItem = ({item}) => {
    return (
      <View style={styles.historyData}>
        <View style={styles.dateContainer}>
          <Text style={styles.dateText}>
            {Moment(item.transdate).format('DD MMM yyy')}
          </Text>
          <View style={styles.hourContainer}>
            <Text style={styles.hourText}>
              {Moment(item.start_hour).format('HH:mm')}
              {' - '}
              {Moment(item.end_hour).format('HH:mm')}
            </Text>
          </View>
        </View>
        <View style={styles.descContainer}>
          <Text numberOfLines={3} style={styles.descTitle}>
            {item.reason}
          </Text>
        </View>
        <View style={[styles.typeContainer]}>
          {item.approval_1 == 1 ? (
            <FAIcon
              name="check-circle"
              size={24}
              style={[styles.typeContent, {color: '#196F3D'}]}
            />
          ) : item.approval_1 == 2 ? (
            <FAIcon
              name="times-circle"
              size={24}
              style={[styles.typeContent, {color: '#bf151e'}]}
            />
          ) : (
            <FAIcon
              name="circle-o"
              size={24}
              style={[styles.typeContent, {color: '#d1d1d1'}]}
            />
          )}
          {item.approval_2 == 1 ? (
            <FAIcon
              name="check-circle"
              size={24}
              style={[styles.typeContent, {color: '#196F3D'}]}
            />
          ) : item.approval_2 == 2 ? (
            <FAIcon
              name="times-circle"
              size={24}
              style={[styles.typeContent, {color: '#bf151e'}]}
            />
          ) : (
            <FAIcon
              name="circle-o"
              size={24}
              style={[styles.typeContent, {color: '#d1d1d1'}]}
            />
          )}
          <TouchableOpacity onPress={() => handleDelete(item.id)}>
            <FAIcon
              name="trash-o"
              size={24}
              style={[styles.typeContent, {color: '#bf151e'}]}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  handleCreate = async () => {
    setLoading(true);

    let date = Moment(transdate).format('YYYY-MM-DD');
    let start_hour = Moment(startHour).format('HH:mm');
    let end_hour = Moment(endHour).format('HH:mm');

    await server
      .post('overtimes/create', {transdate: date, start_hour, end_hour, reason})
      .then((response) => {
        setData(response.data.data);
      })
      .catch((error) => {
        alert(error);
        console.log(error);
      });

    setLoading(false);
  };

  handleDelete = async (id) => {
    await server
      .post('overtimes/deleted', {id})
      .then((response) => {
        setData(response.data.data);
      })
      .catch((error) => {
        alert(error);
        console.log(error);
      });
  };

  buttonOrLoading = () => {
    if (loading) {
      return (
        <View style={[styles.buttonStyle, {marginLeft: 5}]}>
          <ActivityIndicator size="small" color="gold" />
        </View>
      );
    }
    return (
      <TouchableOpacity
        style={[styles.buttonStyle, {marginLeft: 5}]}
        onPress={this.handleCreate}>
        <Text style={styles.buttonText}>Save</Text>
      </TouchableOpacity>
    );
  };

  const showDatePicker = () => {
    setShowDate(true);
  };

  const hideDatePicker = () => {
    setShowDate(false);
  };

  const handleDate = (date) => {
    setTransDate(date);
    hideDatePicker();
  };

  const showStartHourPicker = () => {
    setShowStartHour(true);
  };

  const hideStartHourPicker = () => {
    setShowStartHour(false);
  };

  const handleStartHour = (date) => {
    setStartHour(date);
    hideStartHourPicker();
  };

  const showEndHourPicker = () => {
    setShowEndHour(true);
  };

  const hideEndHourPicker = () => {
    setShowEndHour(false);
  };

  const handleEndHour = (date) => {
    setEndHour(date);
    hideEndHourPicker();
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.gravatarContent}>
          <View style={[styles.avatar, styles.avatarContainer, {margin: 10}]}>
            <Image style={styles.avatar} source={photo} />
          </View>
          <View style={styles.employeeContainer}>
            <Text numberOfLines={1} style={styles.name}>
              {name}
            </Text>
            <Text style={styles.companyName}>{company}</Text>
          </View>
        </View>
        <View style={styles.createContent}>
          <View style={[styles.inputContainer, styles.textContainer]}>
            <TouchableOpacity onPress={showDatePicker}>
              <Text style={styles.inputText}>
                {Moment(transdate).format('YYYY-MM-DD')}
              </Text>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={showDate}
              mode="date"
              onConfirm={handleDate}
              onCancel={hideDatePicker}
              date={transdate}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <View
              style={[
                styles.inputContainer,
                styles.textContainer,
                {width: '40%', marginLeft: 25},
              ]}>
              <TouchableOpacity onPress={showStartHourPicker}>
                <Text style={styles.inputText}>
                  {Moment(startHour).format('HH:mm')}
                </Text>
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={showStartHour}
                mode="time"
                onConfirm={handleStartHour}
                onCancel={hideStartHourPicker}
                date={startHour}
              />
            </View>
            <View
              style={[
                styles.inputContainer,
                styles.textContainer,
                {width: '40%', marginLeft: 5},
              ]}>
              <TouchableOpacity onPress={showEndHourPicker}>
                <Text style={styles.inputText}>
                  {Moment(endHour).format('HH:mm')}
                </Text>
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={showEndHour}
                mode="time"
                onConfirm={handleEndHour}
                onCancel={hideEndHourPicker}
                date={endHour}
              />
            </View>
          </View>
          <View style={[styles.inputContainer, styles.textContainer]}>
            <TextInput
              style={styles.inputText}
              placeholder={'Reason'}
              returnKeyType={'next'}
              autoCorrect={false}
              placeholderTextColor={'#b1b1b1'}
              underlineColorAndroid={'transparent'}
              value={reason}
              onChangeText={(text) => setReason(text)}
            />
          </View>
          <View style={[styles.inputContainer, styles.buttonContainer]}>
            {buttonOrLoading()}
          </View>
        </View>
        <View style={[styles.historyContent]}>
          <Text style={styles.historyTitle}>Unapproved Overtime</Text>
          <FlatList
            data={data}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderItem}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default overtime;
