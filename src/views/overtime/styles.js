import {StyleSheet, Dimensions} from 'react-native';
const {Height} = Dimensions.get('window') - 190;
const {width: WIDTH} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#faf5e6',
    flex: 1,
  },
  content: {
    flex: 1,
    alignItems: 'flex-start',
    width: '100%',
  },
  gravatarContent: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 10,
    height: 140,
  },
  avatarContainer: {
    borderColor: '#FFFFFF',
    borderWidth: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderWidth: 4,
    borderColor: '#FFFFFF',
    borderRadius: 75,
    width: 120,
    height: 120,
  },
  employeeContainer: {
    paddingLeft: 10,
    paddingRight: 10,
    width: '60%',
  },
  name: {
    alignSelf: 'center',
    fontSize: 22,
    color: '#000000',
    fontWeight: '600',
  },
  companyName: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#000000',
    fontWeight: '400',
  },

  createContent: {
    width: '100%',
  },
  inputContainer: {
    marginTop: 10,
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    marginHorizontal: 25,
  },
  textContainer: {
    backgroundColor: 'rgba(211, 165, 13, 0.35)',
  },
  inputText: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingTop: 12,
    paddingLeft: 35,
    paddingRight: 35,
    color: '#636363',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonStyle: {
    backgroundColor: '#D3A40E',
    justifyContent: 'center',
    width: WIDTH - 55,
    borderRadius: 25,
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },

  historyContent: {
    width: '100%',
    flex: 1,
  },
  historyTitle: {
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 5,
    fontSize: 18,
    fontWeight: 'bold',
    backgroundColor: '#f1e3b6',
    color: '#64625c',
  },
  historyData: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#faf5e6',
    borderBottomWidth: 1,
    borderBottomColor: '#d3a40e',
    color: '#767676',
    flexDirection: 'row',
  },
  dateContainer: {
    borderRadius: 10,
    padding: 10,
    width: 150,
    backgroundColor: '#a8830b',
    alignItems: 'center',
  },
  dateText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#faf5e6',
  },
  hourContainer: {
    borderRadius: 10,
    marginLeft: 10,
    width: 100,
    alignItems: 'center',
    flexDirection: 'row',
  },
  hourText: {
    fontSize: 12,
    color: '#faf5e6',
  },
  descContainer: {
    padding: 10,
    width: 100,
  },
  typeContainer: {
    padding: 10,
    alignItems: 'center',
    borderRadius: 10,
    marginLeft: 10,
    flex: 1,
    flexDirection: 'row',
  },
  typeContent: {
    marginRight: 10,
  },
});

export default styles;
