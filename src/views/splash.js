import React, {Component} from 'react';
import {ImageBackground, StyleSheet} from 'react-native';

// Assets
import backgroundImage from '../assets/images/splash.jpg';

export default class splash extends Component {
  render = () => {
    return (
      <ImageBackground
        source={backgroundImage}
        style={styles.container}></ImageBackground>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
