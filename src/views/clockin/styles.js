import {StyleSheet, Dimensions} from 'react-native';
const {Height} = Dimensions.get('window') - 190;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#faf5e6',
    height: '100%',
  },
  content: {
    flex: 1,
    alignItems: 'flex-start',
    width: '100%',
  },
  gravatarContent: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 10,
    height: 140,
  },
  avatarContainer: {
    borderColor: '#FFFFFF',
    borderWidth: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderWidth: 4,
    borderColor: '#FFFFFF',
    borderRadius: 75,
    width: 120,
    height: 120,
  },
  employeeContainer: {
    paddingLeft: 10,
    paddingRight: 10,
    width: '60%',
  },
  name: {
    alignSelf: 'center',
    fontSize: 22,
    color: '#000000',
    fontWeight: '600',
  },
  companyName: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#000000',
    fontWeight: '400',
  },

  buttonContent: {
    width: '100%',
    flexDirection: 'row',
    height: 50,
  },
  buttonItem: {
    flex: 1,
    borderRadius: 7,
    height: 50,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '600',
  },
  historyContent: {
    width: '100%',
  },
  historyTitle: {
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 5,
    fontSize: 18,
    fontWeight: 'bold',
    backgroundColor: '#f1e3b6',
    color: '#64625c',
  },
  historyData: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#faf5e6',
    borderBottomWidth: 1,
    borderBottomColor: '#d3a40e',
    color: '#767676',
    flexDirection: 'row',
  },
  timeContainer: {
    borderRadius: 10,
    paddingLeft: 50,
    paddingRight: 50,
    paddingTop: 10,
    width: 250,
    backgroundColor: '#a8830b',
    alignItems: 'center',
  },
  timeText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#faf5e6',
  },
  typeContainer: {
    padding: 10,
    alignItems: 'center',
    borderRadius: 10,
    marginLeft: 10,
    flex: 1,
  },
  typeText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#a8830b',
  },
});

export default styles;
