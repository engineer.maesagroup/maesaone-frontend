import React from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import styles from './styles';

// Components
import AuthContext from '../../AuthContext';
import Moment from 'moment';

// Services
import server from '../../services/server';

const clockin = () => {
  const {state} = React.useContext(AuthContext);

  const [name, setName] = React.useState('Guest');
  const [company, setCompany] = React.useState('Please Sign In');
  const [photo, setPhoto] = React.useState({
    uri: 'https://www.maesaone.com/assets/images/user.png',
  });

  const [data, setData] = React.useState(null);

  React.useEffect(() => {
    if (state != null && state.employee != null) {
      setName(state.employee.name);
      setCompany(state.employee.company);
      if (state.employee.photo != null)
        setPhoto({uri: `https://www.maesaone.com/${state.employee.photo}`});
      else setPhoto({uri: 'https://www.maesaone.com/assets/images/user.png'});
    } else {
      setName('Guest');
      setCompany('Please Sign In');
      setPhoto({uri: 'https://www.maesaone.com/assets/images/user.png'});
    }

    const refreshData = async () => {
      await server
        .post('attendances/today')
        .then((response) => {
          setData(response.data.data);
        })
        .catch((error) => {
          alert(error);
          console.log(error);
        });
    };

    refreshData();
  }, [state]);

  renderItem = ({item}) => {
    return (
      <View style={styles.historyData}>
        <View style={styles.timeContainer}>
          <Text style={styles.timeText}>
            {Moment(item.check_time).format('HH:mm:ss')}
          </Text>
        </View>
        {item.attendance_type == 'IN' ? (
          <View style={[styles.typeContainer, {backgroundColor: '#266039'}]}>
            <Text style={[styles.typeText, {color: '#e6ede9'}]}>
              {item.attendance_type}
            </Text>
          </View>
        ) : (
          <View style={[styles.typeContainer, {backgroundColor: '#bf151e'}]}>
            <Text style={[styles.typeText, {color: '#f8e7e8'}]}>
              {item.attendance_type}
            </Text>
          </View>
        )}
      </View>
    );
  };

  handleClock = async (type) => {
    await server
      .post('attendances/clock', {type})
      .then((response) => {
        setData(response.data.data);
      })
      .catch((error) => {
        alert(error);
        console.log(error);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.gravatarContent}>
          <View style={[styles.avatar, styles.avatarContainer, {margin: 10}]}>
            <Image style={styles.avatar} source={photo} />
          </View>
          <View style={styles.employeeContainer}>
            <Text numberOfLines={1} style={styles.name}>
              {name}
            </Text>
            <Text style={styles.companyName}>{company}</Text>
          </View>
        </View>
        <View style={styles.buttonContent}>
          <TouchableOpacity
            style={[styles.buttonItem, {backgroundColor: '#266039'}]}
            onPress={() => handleClock('IN')}>
            <Text style={[styles.buttonText, {color: '#e6ede9'}]}>
              Clock In
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonItem, {backgroundColor: '#bf151e'}]}
            onPress={() => handleClock('OUT')}>
            <Text style={[styles.buttonText, {color: '#f8e7e8'}]}>
              Clock Out
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.historyContent]}>
          <Text style={styles.historyTitle}>Today Activity</Text>
          <FlatList
            data={data}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderItem}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default clockin;
