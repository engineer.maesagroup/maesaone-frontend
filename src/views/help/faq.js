import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {WebView} from 'react-native-webview';

export default class faq extends Component {
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#faf5e6'}}>
        <WebView
          javaScriptEnabled
          domStorageEnabled
          allowFileAccessFromFileURLs
          startInLoadingState
          originWhitelist={['*']}
          mixedContentMode="compatibility"
          source={{uri: 'https://maesaone.com/faq'}}
          style={{flex: 1, backgroundColor: '#faf5e6'}}
        />
      </SafeAreaView>
    );
  }
}
