import React, {Component} from 'react';
import {SafeAreaView, Text} from 'react-native';

export default class manual extends Component {
  render() {
    return (
      <SafeAreaView
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{color: '#000000'}}>Manual Book</Text>
      </SafeAreaView>
    );
  }
}
