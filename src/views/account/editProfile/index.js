import React from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
  ActivityIndicator,
} from 'react-native';
import styles from './style';

// Components
import AuthContext from '../../../AuthContext';

// Services
import server from '../../../services/server';

function editProfile({navigation}) {
  const {state, dispatch} = React.useContext(AuthContext);
  const [email, setEmail] = React.useState('');
  const [mobile, setMobile] = React.useState('');
  const [position, setPosition] = React.useState('');
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (state != null && state.employee != null) {
      setEmail(state.employee.email);
      setMobile(state.employee.mobile);
      setPosition(state.employee.position);
    }
  }, [state]);

  handleSave = async () => {
    Keyboard.dismiss();
    setLoading(true);

    if (email == '') {
      alert('Please fill input email');
      setLoading(false);
      return;
    }

    if (mobile == '') {
      alert('Please fill input mobile');
      setLoading(false);
      return;
    }

    if (position == '') {
      alert('Please fill input position');
      setLoading(false);
      return;
    }

    await server
      .post('employees/profile', {email, mobile, position})
      .then(() => {
        setLoading(false);
        alert('Data Changed');

        // Get Employee Detail
        server
          .get('employees/me')
          .then((response) => {
            let employee = response.data.data.data;
            let task = response.data.data.task;
            dispatch({type: 'RELOAD_EMPLOYEE', employee: employee, task: task});
            navigation.goBack();
          })
          .catch((error) => {
            console.log(error);
            console.log(error.response);
          });
      })
      .catch((error) => {
        setLoading(false);
        console.log(error.response.data.error);
        alert(
          'Login failed. Please try again (' + error.response.data.error + ')',
        );
      });
  };

  buttonOrLoading = () => {
    if (loading) {
      return (
        <View style={styles.inputContainer}>
          <ActivityIndicator size="small" color="gold" />
        </View>
      );
    }
    return (
      <TouchableOpacity
        style={[styles.inputContainer, styles.buttonContainer]}
        onPress={this.handleSave}>
        <Text style={styles.buttonText}>Save</Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.gravatarContainer}>
        <View style={styles.content}>
          <View style={[styles.avatar, styles.avatarContainer, {margin: 10}]}>
            {state == null || state.employee == null ? null : (
              <Image
                style={styles.avatar}
                source={{
                  uri: `https://www.maesaone.com/${state.employee.photo}`,
                }}
              />
            )}
          </View>
          <Text numberOfLines={1} style={styles.title}>
            {state == null || state.employee == null ? '' : state.employee.name}
          </Text>
          <Text style={[styles.subtitle, {marginBottom: 30}]}>
            {state == null || state.employee == null
              ? ''
              : state.employee.company}
          </Text>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Email</Text>
            <TextInput
              style={[styles.inputText, styles.textContainer]}
              placeholder={'Email'}
              returnKeyType={'next'}
              autoCorrect={false}
              placeholderTextColor={'#919191'}
              underlineColorAndroid={'transparent'}
              value={email}
              onChangeText={(text) => setEmail(text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Mobile</Text>
            <TextInput
              style={[styles.inputText, styles.textContainer]}
              placeholder={'Mobile'}
              autoCorrect={false}
              placeholderTextColor={'#919191'}
              underlineColorAndroid={'transparent'}
              value={mobile}
              onChangeText={(text) => setMobile(text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Position</Text>
            <TextInput
              style={[styles.inputText, styles.textContainer]}
              placeholder={'Position'}
              autoCorrect={false}
              placeholderTextColor={'#919191'}
              underlineColorAndroid={'transparent'}
              value={position}
              onChangeText={(text) => setPosition(text)}
            />
          </View>
          {buttonOrLoading()}
        </View>
      </View>
    </SafeAreaView>
  );
}

export default editProfile;
