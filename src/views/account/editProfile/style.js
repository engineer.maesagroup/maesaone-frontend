import {StyleSheet, Dimensions} from 'react-native';
const {width: WIDTH} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#faf5e6',
  },
  gravatarContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  content: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  avatarContainer: {
    borderColor: '#D3A40E',
    borderWidth: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  avatar: {
    borderWidth: 4,
    borderColor: '#D3A40E',
    borderRadius: 75,
    width: 120,
    height: 120,
  },
  title: {
    alignSelf: 'center',
    fontSize: 22,
    color: '#000000',
    fontWeight: '600',
  },
  subtitle: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#000000',
    fontWeight: '400',
  },

  inputContainer: {
    marginTop: 10,
    width: WIDTH - 20,
    height: 45,
    flexDirection: 'row',
  },
  inputLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingTop: 12,
    paddingLeft: 12,
    width: '40%',
    color: '#636363',
  },
  textContainer: {
    borderRadius: 25,
    backgroundColor: 'rgba(211, 165, 13, 0.35)',
    width: '60%',
    justifyContent: 'center',
  },
  inputText: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingLeft: 20,
    paddingRight: 20,
    color: '#1c1c1c',
  },
  buttonContainer: {
    borderRadius: 25,
    backgroundColor: '#D3A40E',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    paddingTop: 10,
    fontSize: 20,
    textAlign: 'center',
  },
});

export default styles;
