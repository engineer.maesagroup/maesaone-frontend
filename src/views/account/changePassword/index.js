import React from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
  ActivityIndicator,
} from 'react-native';
import styles from './style';

// Components
import AuthContext from '../../../AuthContext';
import FAIcon from 'react-native-vector-icons/FontAwesome';

// Services
import server from '../../../services/server';

function channgePassword({navigation}) {
  const {state, dispatch} = React.useContext(AuthContext);
  const [current, setCurrent] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [confirm, setConfirm] = React.useState('');
  const [hideCurrent, setHideCurrent] = React.useState(true);
  const [hidePassword, setHidePassword] = React.useState(true);
  const [hideConfirm, setHideConfirm] = React.useState(true);
  const [loading, setLoading] = React.useState(false);

  handleSave = async () => {
    Keyboard.dismiss();
    setLoading(true);

    if (current == '') {
      alert('Please fill current password');
      setLoading(false);
      return;
    }

    if (password == '') {
      alert('Please fill new password');
      setLoading(false);
      return;
    }

    if (confirm == '') {
      alert('Please fill confirm password');
      setLoading(false);
      return;
    }

    await server
      .post('employees/password', {current, password, confirm})
      .then((response) => {
        setLoading(false);
        alert('Password Changed');

        let token = response.data.data.token;
        let lessSecure = response.data.data.lessSecure;
        let employee = response.data.data.data;

        dispatch({
          type: 'SIGN_IN',
          token: token,
          employee: employee,
          lessSecure: lessSecure,
        });

        navigation.goBack();
      })
      .catch((error) => {
        setLoading(false);
        console.log(error.response);
        alert('Change Password Failed (' + error.response.data.error + ')');
      });
  };

  handleCurrent = () => {
    if (hideCurrent) {
      setHideCurrent(false);
    } else {
      setHideCurrent(true);
    }
  };

  handlePassword = () => {
    if (hidePassword) {
      setHidePassword(false);
    } else {
      setHidePassword(true);
    }
  };

  handleConfirm = () => {
    if (hideConfirm) {
      setHideConfirm(false);
    } else {
      setHideConfirm(true);
    }
  };

  buttonOrLoading = () => {
    if (loading) {
      return (
        <View style={[styles.buttonStyle, {marginLeft: 5}]}>
          <ActivityIndicator size="small" color="gold" />
        </View>
      );
    }
    return (
      <TouchableOpacity
        style={[styles.buttonStyle, {marginLeft: 5}]}
        onPress={this.handleSave}>
        <Text style={styles.buttonText}>Save</Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.gravatarContainer}>
        <View style={styles.content}>
          <View style={[styles.avatar, styles.avatarContainer, {margin: 10}]}>
            {state == null || state.employee == null ? null : (
              <Image
                style={styles.avatar}
                source={{
                  uri: `https://www.maesaone.com/${state.employee.photo}`,
                }}
              />
            )}
          </View>
          <Text numberOfLines={1} style={styles.title}>
            {state == null || state.employee == null ? '' : state.employee.name}
          </Text>
          <Text style={[styles.subtitle, {marginBottom: 30}]}>
            {state == null || state.employee == null
              ? ''
              : state.employee.company}
          </Text>
          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.inputText, styles.textContainer]}
              placeholder={'Current Password'}
              returnKeyType={'next'}
              autoCorrect={false}
              placeholderTextColor={'#919191'}
              underlineColorAndroid={'transparent'}
              secureTextEntry={hideCurrent}
              value={current}
              onChangeText={(text) => setCurrent(text)}
            />
            <TouchableOpacity
              style={styles.eyeContainer}
              onPress={this.handleCurrent}>
              <FAIcon
                style={styles.eyeIcon}
                name={hideCurrent ? 'eye-slash' : 'eye'}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.inputText, styles.textContainer]}
              placeholder={'New Password'}
              autoCorrect={false}
              placeholderTextColor={'#919191'}
              underlineColorAndroid={'transparent'}
              secureTextEntry={hidePassword}
              value={password}
              onChangeText={(text) => setPassword(text)}
            />
            <TouchableOpacity
              style={styles.eyeContainer}
              onPress={this.handlePassword}>
              <FAIcon
                style={styles.eyeIcon}
                name={hidePassword ? 'eye-slash' : 'eye'}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.inputText, styles.textContainer]}
              placeholder={'ConfirmPassword'}
              autoCorrect={false}
              placeholderTextColor={'#919191'}
              underlineColorAndroid={'transparent'}
              secureTextEntry={hideConfirm}
              value={confirm}
              onChangeText={(text) => setConfirm(text)}
            />
            <TouchableOpacity
              style={styles.eyeContainer}
              onPress={this.handleConfirm}>
              <FAIcon
                style={styles.eyeIcon}
                name={hideConfirm ? 'eye-slash' : 'eye'}
              />
            </TouchableOpacity>
          </View>
          <View style={[styles.inputContainer, styles.buttonContainer]}>
            <TouchableOpacity
              style={[styles.buttonStyle, {marginRight: 5}]}
              onPress={() => navigation.push('Home')}>
              <Text style={styles.buttonText}>
                <FAIcon style={styles.buttonIcon} name={'arrow-circle-left'} />
                {'  '}
                Home
              </Text>
            </TouchableOpacity>
            {buttonOrLoading()}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default channgePassword;
