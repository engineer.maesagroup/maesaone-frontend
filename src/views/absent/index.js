import React from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
  TextInput,
  ActivityIndicator,
  Button,
} from 'react-native';
import styles from './styles';

// Components
import AuthContext from '../../AuthContext';
import Moment from 'moment';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

// Services
import server from '../../services/server';

const absent = () => {
  const {state} = React.useContext(AuthContext);

  const [name, setName] = React.useState('Guest');
  const [company, setCompany] = React.useState('Please Sign In');
  const [photo, setPhoto] = React.useState({
    uri: 'https://www.maesaone.com/assets/images/user.png',
  });

  const [startDate, setStartDate] = React.useState(new Date());
  const [endDate, setEndDate] = React.useState(new Date());
  const [reason, setReason] = React.useState('');

  const [showStartDate, setShowStartDate] = React.useState(false);
  const [showEndDate, setShowEndDate] = React.useState(false);

  const [data, setData] = React.useState(null);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (state != null && state.employee != null) {
      setName(state.employee.name);
      setCompany(state.employee.company);
      if (state.employee.photo != null)
        setPhoto({uri: `https://www.maesaone.com/${state.employee.photo}`});
      else setPhoto({uri: 'https://www.maesaone.com/assets/images/user.png'});
    } else {
      setName('Guest');
      setCompany('Please Sign In');
      setPhoto({uri: 'https://www.maesaone.com/assets/images/user.png'});
    }

    const refreshData = async () => {
      await server
        .post('unattendances/unapproved')
        .then((response) => {
          setData(response.data.data);
        })
        .catch((error) => {
          alert(error);
          console.log(error);
        });
    };

    refreshData();
  }, [state]);

  renderItem = ({item}) => {
    return (
      <View style={styles.historyData}>
        <View style={styles.dateContainer}>
          <Text style={styles.dateText}>
            {Moment(item.start_date).format('DD MMM YYYY HH:mm')}
          </Text>
          <Text style={styles.dateText}>
            {Moment(item.end_date).format('DD MMM YYYY HH:mm')}
          </Text>
        </View>
        <View style={styles.descContainer}>
          <Text numberOfLines={3} style={styles.descTitle}>
            {item.reason}
          </Text>
        </View>
        <View style={[styles.typeContainer]}>
          {item.approval_1 == 1 ? (
            <FAIcon
              name="check-circle"
              size={24}
              style={[styles.typeContent, {color: '#196F3D'}]}
            />
          ) : item.approval_1 == 2 ? (
            <FAIcon
              name="times-circle"
              size={24}
              style={[styles.typeContent, {color: '#bf151e'}]}
            />
          ) : (
            <FAIcon
              name="circle-o"
              size={24}
              style={[styles.typeContent, {color: '#d1d1d1'}]}
            />
          )}
          {item.approval_2 == 1 ? (
            <FAIcon
              name="check-circle"
              size={24}
              style={[styles.typeContent, {color: '#196F3D'}]}
            />
          ) : item.approval_2 == 2 ? (
            <FAIcon
              name="times-circle"
              size={24}
              style={[styles.typeContent, {color: '#bf151e'}]}
            />
          ) : (
            <FAIcon
              name="circle-o"
              size={24}
              style={[styles.typeContent, {color: '#d1d1d1'}]}
            />
          )}
          <TouchableOpacity onPress={() => handleDelete(item.id)}>
            <FAIcon
              name="trash-o"
              size={24}
              style={[styles.typeContent, {color: '#bf151e'}]}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  handleCreate = async () => {
    setLoading(true);

    let transdate = Moment(startDate).format('YYYY-MM-DD');
    let start_date = Moment(startDate).format('YYYY-MM-DD HH:mm');
    let end_date = Moment(endDate).format('YYYY-MM-DD HH:mm');

    await server
      .post('unattendances/create', {
        transdate,
        start_date,
        end_date,
        reason,
      })
      .then((response) => {
        setData(response.data.data);
      })
      .catch((error) => {
        alert(error);
        console.log(error);
      });

    setLoading(false);
  };

  handleDelete = async (id) => {
    await server
      .post('unattendances/deleted', {id})
      .then((response) => {
        setData(response.data.data);
      })
      .catch((error) => {
        alert(error);
        console.log(error);
      });
  };

  buttonOrLoading = () => {
    if (loading) {
      return (
        <View style={[styles.buttonStyle, {marginLeft: 5}]}>
          <ActivityIndicator size="small" color="gold" />
        </View>
      );
    }
    return (
      <TouchableOpacity
        style={[styles.buttonStyle, {marginLeft: 5}]}
        onPress={this.handleCreate}>
        <Text style={styles.buttonText}>Save</Text>
      </TouchableOpacity>
    );
  };

  const showStartDatePicker = () => {
    setShowStartDate(true);
  };

  const hideStartDatePicker = () => {
    setShowStartDate(false);
  };

  const handleStartDate = (date) => {
    setStartDate(date);
    hideStartDatePicker();
  };

  const showEndDatePicker = () => {
    setShowEndDate(true);
  };

  const hideEndDatePicker = () => {
    setShowEndDate(false);
  };

  const handleEndDate = (date) => {
    setEndDate(date);
    hideEndDatePicker();
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.gravatarContent}>
          <View style={[styles.avatar, styles.avatarContainer, {margin: 10}]}>
            <Image style={styles.avatar} source={photo} />
          </View>
          <View style={styles.employeeContainer}>
            <Text numberOfLines={1} style={styles.name}>
              {name}
            </Text>
            <Text style={styles.companyName}>{company}</Text>
          </View>
        </View>
        <View style={styles.createContent}>
          <View style={[styles.inputContainer, styles.textContainer]}>
            <TouchableOpacity onPress={showStartDatePicker}>
              <Text style={styles.inputText}>
                {Moment(startDate).format('YYYY-MM-DD HH:mm')}
              </Text>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={showStartDate}
              mode="datetime"
              onConfirm={handleStartDate}
              onCancel={hideStartDatePicker}
              date={startDate}
            />
          </View>
          <View style={[styles.inputContainer, styles.textContainer]}>
            <TouchableOpacity onPress={showEndDatePicker}>
              <Text style={styles.inputText}>
                {Moment(endDate).format('YYYY-MM-DD HH:mm')}
              </Text>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={showEndDate}
              mode="datetime"
              onConfirm={handleEndDate}
              onCancel={hideEndDatePicker}
              date={endDate}
            />
          </View>
          <View style={[styles.inputContainer, styles.textContainer]}>
            <TextInput
              style={styles.inputText}
              placeholder={'Reason'}
              returnKeyType={'next'}
              autoCorrect={false}
              placeholderTextColor={'#b1b1b1'}
              underlineColorAndroid={'transparent'}
              value={reason}
              onChangeText={(text) => setReason(text)}
            />
          </View>
          <View style={[styles.inputContainer, styles.buttonContainer]}>
            {buttonOrLoading()}
          </View>
        </View>
        <View style={[styles.historyContent]}>
          <Text style={styles.historyTitle}>Unapproved Absent</Text>
          <FlatList
            data={data}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderItem}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default absent;
