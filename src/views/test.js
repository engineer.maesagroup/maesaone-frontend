import React from 'react';
import {View, Image} from 'react-native';

import {AuthContext} from '../App';
import ImagePicker from 'react-native-image-picker';
import {TouchableOpacity} from 'react-native-gesture-handler';

// Services
import {fileServer} from '../services/server';

function test({navigation}) {
  const {state, dispatch} = React.useContext(AuthContext);
  const [avatar, setAvatar] = React.useState({
    uri: `https://www.maesaone.com/assets/images/user.png`,
  });

  React.useEffect(() => {
    if (state == null || state.employee == null) {
      navigation.navigate('Login');
    }
    // setAvatar({
    //   uri: `https://www.maesaone.com/${state.employee.photo}`,
    // });
  }, [state]);

  handleChangeImage = () => {
    ImagePicker.showImagePicker(
      {
        title: 'Select Avatar',
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      },
      (response) => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          setAvatar({uri: response.uri});

          var data = new FormData();
          data.append('image', {
            uri: response.uri,
            name: 'userProfile.jpg',
            type: response.type,
          });
          console.log(response.uri);
          console.log(response.type);

          fileServer
            .post('employees/my-avatar', data)
            .then((response) => {
              console.log('sukses');
              console.log(response.data.data);
              //       let token = response.data.data.token;
              //       let employee = response.data.data.data;
              //       let lessSecure = response.data.data.lessSecure;

              //       // dispatch({
              //       //   type: 'SIGN_IN',
              //       //   token: token,
              //       //   employee: employee,
              //       //   lessSecure: lessSecure,
              //       // });
            })
            .catch((error) => {
              console.log('error');
              console.log(error);
              // console.log(error.response.data.error);
              //   alert(error.response.data.error);
            });
        }
      },
    );
  };

  return (
    <View>
      <TouchableOpacity onPress={handleChangeImage}>
        <View style={{width: 100, height: 100}}>
          <Image style={{width: 100, height: 100}} source={avatar} />
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default test;
