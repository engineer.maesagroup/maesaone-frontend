import React, {Component} from 'react';
import {ScrollView, RefreshControl} from 'react-native';

// Components
import Gravatar from '../components/gravatar';
import Feeds from '../components/feeds';
import AuthContext from '../AuthContext';

// Services
import server from '../services/server';

const home = ({navigation}) => {
  const {dispatch} = React.useContext(AuthContext);
  const [refreshing, setRefreshing] = React.useState(false);

  newsHeaderPress = () => {
    navigation.navigate('News', {type: 'News'});
  };

  trainingHeaderPress = () => {
    navigation.navigate('Training', {type: 'Training'});
  };

  const onRefresh = React.useCallback(() => {
    // Get Employee Detail
    setRefreshing(true);
    server
      .get('employees/me')
      .then((response) => {
        let employee = response.data.data.data;
        let task = response.data.data.task;
        dispatch({type: 'RELOAD_EMPLOYEE', employee: employee, task: task});
      })
      .catch((error) => {
        console.log(error);
        console.log(error.response);
      });
    setRefreshing(false);
  }, []);

  return (
    <ScrollView
      style={{backgroundColor: '#faf5e6'}}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <Gravatar navigation={navigation} />
      <Feeds
        title="Maesa News Update"
        type="News"
        onPress={newsHeaderPress}
        navigation={navigation}
      />
      <Feeds
        title="Dua Tangan Indonesia | Training"
        type="Training"
        onPress={trainingHeaderPress}
        navigation={navigation}
      />
    </ScrollView>
  );
};

export default home;
