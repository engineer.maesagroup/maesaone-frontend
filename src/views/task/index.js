import React from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
  Modal,
} from 'react-native';
import styles from './styles';

// Components
import AuthContext from '../../AuthContext';
import Moment from 'moment';

// Services
import server from '../../services/server';

const task = () => {
  const {state, dispatch} = React.useContext(AuthContext);
  const [overtimeData, setOvertimeData] = React.useState(null);
  const [unattendanceData, setUnattendanceData] = React.useState(null);
  const [imageVisible, setImageVisible] = React.useState(false);
  const [photo, setPhoto] = React.useState({
    uri: 'https://www.maesaone.com/assets/images/user.png',
  });

  React.useEffect(() => {
    const refreshData = async () => {
      await server
        .post('overtimes/approval')
        .then((response) => {
          setOvertimeData(response.data.data);
        })
        .catch((error) => {
          alert(error);
          console.log(error);
        });

      await server
        .post('unattendances/approval')
        .then((response) => {
          setUnattendanceData(response.data.data);
        })
        .catch((error) => {
          alert(error);
          console.log(error);
        });
    };

    refreshData();
  }, [state]);

  handleImageClick = (photo) => {
    setImageVisible(true);
    setPhoto(photo);
  };

  handleOvertime = async (id, status) => {
    await server
      .post('overtimes/approve', {id, status})
      .then((response) => {
        setOvertimeData(response.data.data);
      })
      .catch((error) => {
        alert(error);
        console.log(error);
      });

    await refreshState();
  };

  handleAbsent = async (id, status) => {
    await server
      .post('unattendances/approve', {id, status})
      .then((response) => {
        setUnattendanceData(response.data.data);
      })
      .catch((error) => {
        alert(error);
        console.log(error);
      });

    await refreshState();
  };

  refreshState = async () => {
    // Get Employee Detail
    await server
      .get('employees/me')
      .then((response) => {
        let employee = response.data.data.data;
        let task = response.data.data.task;
        dispatch({type: 'RELOAD_EMPLOYEE', employee: employee, task: task});
      })
      .catch((error) => {
        console.log(error);
        console.log(error.response);
      });
  };

  renderOvertime = ({item}) => {
    let photo = {uri: 'https://www.maesaone.com/assets/images/user.png'};
    if (item.photo != null && item.photo != '')
      photo = {uri: 'https://www.maesaone.com/' + item.photo};

    return (
      <View style={styles.historyData}>
        <TouchableOpacity
          onPress={() => {
            handleImageClick(photo);
          }}>
          <View style={styles.gravatarContent}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                {marginRight: 10},
              ]}>
              <Image style={styles.avatar} source={photo} />
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.detailContainer}>
          <Text style={styles.detailText}>
            {Moment(item.transdate).format('DD MMM')}
          </Text>
          <Text style={styles.detailText}>
            {Moment(item.start_hour).format('HH:mm')}
            {' - '}
            {Moment(item.end_hour).format('HH:mm')}
          </Text>
          <Text numberOfLines={1} style={styles.detailText}>
            {item.reason}
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={[
              styles.buttonStyle,
              {backgroundColor: '#266039', marginBottom: 5},
            ]}
            onPress={() => handleOvertime(item.id, 1)}>
            <Text style={[styles.buttonText, {color: '#e6ede9'}]}>Approve</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.buttonStyle,
              {backgroundColor: '#bf151e', marginTop: 5},
            ]}
            onPress={() => handleOvertime(item.id, 2)}>
            <Text style={[styles.buttonText, {color: '#f8e7e8'}]}>Reject</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderAbsent = ({item}) => {
    let photo = {uri: 'https://www.maesaone.com/assets/images/user.png'};
    if (item.photo != null && item.photo != '')
      photo = {uri: 'https://www.maesaone.com/' + item.photo};

    return (
      <View style={styles.historyData}>
        <TouchableOpacity
          onPress={() => {
            handleImageClick(photo);
          }}>
          <View style={styles.gravatarContent}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                {marginRight: 10},
              ]}>
              <Image style={styles.avatar} source={photo} />
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.detailContainer}>
          <Text style={styles.detailText}>
            {Moment(item.start_date).format('DD MMM YYYY HH:mm')}
          </Text>
          <Text style={styles.detailText}>
            {Moment(item.end_date).format('DD MMM YYYY HH:mm')}
          </Text>
          <Text numberOfLines={1} style={styles.detailText}>
            {item.reason}
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={[
              styles.buttonStyle,
              {backgroundColor: '#266039', marginBottom: 5},
            ]}
            onPress={() => handleAbsent(item.id, 1)}>
            <Text style={[styles.buttonText, {color: '#e6ede9'}]}>Approve</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.buttonStyle,
              {backgroundColor: '#bf151e', marginTop: 5},
            ]}
            onPress={() => handleAbsent(item.id, 2)}>
            <Text style={[styles.buttonText, {color: '#f8e7e8'}]}>Reject</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={[styles.historyContent]}>
          <Text style={styles.historyTitle}>Unapproved Overtime</Text>
          <FlatList
            data={overtimeData}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderOvertime}
          />
        </View>
        <View style={[styles.historyContent]}>
          <Text style={styles.historyTitle}>Unapproved Absent</Text>
          <FlatList
            data={unattendanceData}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderAbsent}
          />
        </View>
      </View>
      <Modal animationType="slide" transparent={true} visible={imageVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.gravatarContent}>
              <View
                style={[styles.avatar, styles.avatarContainer, {margin: 10}]}>
                <Image
                  style={[styles.avatar, {width: 300, height: 300}]}
                  source={photo}
                />
              </View>
            </View>
          </View>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => {
              setImageVisible(!imageVisible);
            }}>
            <Text style={styles.textStyle}>Close</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

export default task;
