import {StyleSheet, Dimensions} from 'react-native';
const {Height} = Dimensions.get('window') - 190;
const {width: WIDTH} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#faf5e6',
    flex: 1,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  historyContent: {
    width: '100%',
    flex: 1,
  },
  historyTitle: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 5,
    fontSize: 18,
    fontWeight: 'bold',
    backgroundColor: '#f1e3b6',
    color: '#64625c',
  },
  historyData: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#faf5e6',
    borderBottomWidth: 1,
    borderBottomColor: '#d3a40e',
    color: '#767676',
    flexDirection: 'row',
  },

  gravatarContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarContainer: {
    borderColor: '#FFFFFF',
    borderWidth: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderWidth: 4,
    borderColor: '#FFFFFF',
    borderRadius: 75,
    width: 60,
    height: 60,
    backgroundColor: '#123456',
  },

  detailContainer: {
    borderRadius: 10,
    padding: 10,
    width: 200,
    backgroundColor: '#a8830b',
    alignItems: 'center',
  },
  detailText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#faf5e6',
  },

  buttonContainer: {
    justifyContent: 'center',
    flex: 1,
    paddingLeft: 10,
  },
  buttonStyle: {
    backgroundColor: '#D3A40E',
    justifyContent: 'center',
    borderRadius: 5,
    flex: 1,
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    margin: 10,
    backgroundColor: '#faf5e6',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  closeButton: {
    backgroundColor: '#D3A40E',
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default styles;
