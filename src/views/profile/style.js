import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#faf5e6',
  },
  gravatarContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  imageBackground: {
    width: '100%',
    height: '100%',
  },
  content: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  avatarContainer: {
    borderColor: '#D3A40E',
    borderWidth: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  avatar: {
    borderWidth: 4,
    borderColor: '#D3A40E',
    borderRadius: 75,
    width: 120,
    height: 120,
  },
  title: {
    alignSelf: 'center',
    fontSize: 22,
    color: '#000000',
    fontWeight: '600',
  },
  subtitle: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#000000',
    fontWeight: '400',
  },
  detail: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  detailIcon: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#000000',
    fontWeight: '200',
    marginRight: 5,
  },
  detailText: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#000000',
    fontWeight: '400',
  },
  version: {
    fontSize: 9,
    marginTop: 30,
    alignSelf: 'center',
    paddingBottom: 20,
  },
});

export default styles;
