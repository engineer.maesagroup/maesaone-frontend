import React, {useContext} from 'react';
import {View, Text, Image, SafeAreaView} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import styles from './style';

// Components
import {Group, Item} from '../../components/menu';
import AuthContext from '../../AuthContext';
import config from '../../config.json';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import {ScrollView} from 'react-native-gesture-handler';

function profile({navigation}) {
  const {state, dispatch} = useContext(AuthContext);
  const [avatar, setAvatar] = React.useState({
    uri: `https://www.maesaone.com/assets/images/user.png`,
  });

  React.useEffect(() => {
    if (state == null || state.employee == null) {
      navigation.navigate('Login');
    } else {
      if (state.employee.photo != null)
        setAvatar({
          uri: `https://www.maesaone.com/${state.employee.photo}`,
        });
      else
        setAvatar({
          uri: `https://www.maesaone.com/assets/images/user.png`,
        });
    }
  }, [state]);

  useFocusEffect(
    React.useCallback(() => {
      if (state == null || state.employee == null) {
        navigation.navigate('Login');
      } else {
        if (state.employee.photo != null)
          setAvatar({
            uri: `https://www.maesaone.com/${state.employee.photo}`,
          });
        else
          setAvatar({
            uri: `https://www.maesaone.com/assets/images/user.png`,
          });
      }
    }, [state]),
  );

  handleSignOut = async () => {
    await dispatch({type: 'SIGN_OUT'});
    navigation.navigate('Home');
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <View style={styles.gravatarContainer}>
          <View style={styles.content}>
            <View style={[styles.avatar, styles.avatarContainer, {margin: 10}]}>
              {state == null || state.employee == null ? null : (
                <Image style={styles.avatar} source={avatar} />
              )}
            </View>
            <Text numberOfLines={1} style={styles.title}>
              {state == null || state.employee == null
                ? ''
                : state.employee.name}
            </Text>
            <Text style={styles.subtitle}>
              {state == null || state.employee == null
                ? ''
                : state.employee.company}
            </Text>
            <Text style={styles.subtitle}>
              {state == null || state.employee == null
                ? ''
                : state.employee.position}
            </Text>
            <View style={styles.detail}>
              <FAIcon style={styles.detailIcon} name={'phone'} />
              <Text style={styles.detailText}>
                {state == null || state.employee == null
                  ? ''
                  : state.employee.mobile}
              </Text>
            </View>
            <View style={styles.detail}>
              <FAIcon style={styles.detailIcon} name={'address-card'} />
              <Text style={styles.detailText}>
                {state == null || state.employee == null
                  ? ''
                  : state.employee.email}
              </Text>
            </View>
          </View>
        </View>
        <Group text="Account" />
        <Item
          text="Edit Profile"
          onPress={() => navigation.navigate('Edit Profile')}
        />
        <Item
          text="Change Password"
          onPress={() => navigation.navigate('Change Password')}
        />
        <Item text="Logout" onPress={this.handleSignOut} />
        <Group text="Help" />
        <Item
          text="Term and Condition"
          onPress={() => navigation.navigate('TAC')}
        />
        <Item text="Privacy Policy" onPress={() => navigation.navigate('PP')} />
        <Item
          text="Frequently Asked Questions"
          onPress={() => navigation.navigate('FAQ')}
        />
        <Item text="Contact" onPress={() => navigation.navigate('Contact')} />
        <Text style={styles.version}>{config.version}</Text>
      </ScrollView>
    </SafeAreaView>
  );
}

export default profile;
