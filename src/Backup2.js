import * as React from 'react';
import {SafeAreaView, Text} from 'react-native';

// Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Components
import FAIcon from 'react-native-vector-icons/FontAwesome';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';

// Views
import SplashScreen from './views/splash';
import HomeScreen from './views/home';
import ProfileScreen from './views/profile';
import LoginScreen from './views/login';

// Account Page
import EditProfileScreen from './views/account/editProfile';
import ChangePasswordScreen from './views/account/changePassword';

// Help Page
import ManualScreen from './views/help/manual';
import TACScreen from './views/help/tac';
import PPScreen from './views/help/pp';
import FAQScreen from './views/help/faq';
import HelpdeskScreen from './views/help/helpdesk';

const AuthContext = React.createContext();
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function PayslipScreen() {
  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>PayslipScreen!</Text>
    </SafeAreaView>
  );
}

function ClockInScreen() {
  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>ClockInScreen!</Text>
    </SafeAreaView>
  );
}

function KPIScreen() {
  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>KPIScreen!</Text>
    </SafeAreaView>
  );
}

function AppStack() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'Payslip') {
            iconName = 'cash-multiple';
          } else if (route.name == 'History') {
            iconName = 'clipboard-check-outline';
          } else if (route.name == 'KPI') {
            iconName = 'account-key';
          } else if (route.name == 'Profile') {
            return <FAIcon name={'user-circle'} size={20} color={color} />;
          }

          return <MCIcon name={iconName} size={24} color={color} />;
        },
      })}
      tabBarOptions={{
        labelStyle: {fontSize: 14, fontWeight: 'bold'},
        activeBackgroundColor: '#D3A40E',
        inactiveBackgroundColor: '#ffffff',
        activeTintColor: '#ffffff',
        inactiveTintColor: '#636363',
      }}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Payslip" component={PayslipScreen} />
      <Tab.Screen name="History" component={ClockInScreen} />
      <Tab.Screen name="KPI" component={KPIScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
}

function App() {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            token: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            // isSignout: false,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            // isSignout: false,
            userToken: null,
            isLoading: false,
          };
      }
    },
    {
      isLoading: true,
      token: null,
      user: null,
    },
  );

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let token = null;

      token = await AsyncStorage.getItem('token');
      if (token != null) {
        // Validate in the server
      }

      await performTimeConsumingTask();
      dispatch({type: 'RESTORE_TOKEN', token: token});
    };

    bootstrapAsync();
  }, []);

  performTimeConsumingTask = async () => {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve('result');
      }, 500),
    );
  };

  const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token

        dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
      },
      signOut: () => dispatch({type: 'SIGN_OUT'}),
    }),
    [],
  );

  if (state.isLoading) {
    return <SplashScreen />;
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="App" component={AppStack} />
          <Stack.Screen name="Login" component={LoginScreen} />

          <Stack.Screen name="Edit Profile" component={EditProfileScreen} />
          <Stack.Screen
            name="Change Password"
            component={ChangePasswordScreen}
          />

          <Stack.Screen name="Manual" component={ManualScreen} />
          <Stack.Screen name="TAC" component={TACScreen} />
          <Stack.Screen name="PP" component={PPScreen} />
          <Stack.Screen name="FAQ" component={FAQScreen} />
          <Stack.Screen name="Helpdesk" component={HelpdeskScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}

export default App;
export {AuthContext};
